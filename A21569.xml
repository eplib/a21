<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://schemata.earlyprint.org/schemata/tei_earlyprint.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="A21569">
 <teiHeader>
  <fileDesc>
   <titleStmt>
    <title type="_245">By the Kyng and the Quene the King and Quenes maiesties being credible enfourmed that diuerse and many there louing faythfull and obedient subiectes, inhabityng vpon the sea costes, and vsyng traffyque by sea, and dyuers others be very desirous to prepare and esquippe sundry shypes ... for the anoyaunce of there maiesties enemies the  Frenchmen ...</title>
    <author>England and Wales. Sovereign (1553-1558 : Mary I)</author>
   </titleStmt>
   <editionStmt>
    <edition>This text is an enriched version of the TCP digital transcription A21569 of text S3712 in the <ref target="A21569-http;//estc.bl.uk">English Short Title Catalog</ref>. Textual changes  and metadata enrichments aim at making the text more  computationally tractable, easier to read, and suitable for network-based collaborative curation by amateur and professional end users from many walks of life.  The text has been tokenized and linguistically annotated with <ref target="http://morphadorner.northwestern.edu/morphadorner/">MorphAdorner</ref>. The annotation includes standard spellings that support the display of a text in	 a standardized format that preserves archaic forms ('loveth', 'seekest'). Textual changes aim at restoring the text the author or stationer meant to publish.</edition>
    <respStmt>
     <resp>This text has not been fully proofread</resp>
     <name/>
    </respStmt>
   </editionStmt>
   <extent>Approx. 2 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
   <publicationStmt>
    <publisher>EarlyPrint Project</publisher>
    <pubPlace>Evanston,IL, Notre Dame, IN, St. Louis, MO</pubPlace>
    <date>2018</date>
    <idno type="DLPS">A21569</idno>
    <idno type="stc">STC 7876</idno>
    <idno type="stc">ESTC S3712</idno>
    <idno type="eebo_citation">33150827</idno>
    <idno type="oclc">ocm 33150827</idno>
    <idno type="vid">28771</idno>
    <availability>
     <p>To the extent possible under law, the Text Creation Partnership has waived all copyright and related or neighboring rights to this keyboarded and encoded edition of the work described above, according to the terms of the CC0 1.0 Public Domain Dedication (http://creativecommons.org/publicdomain/zero/1.0/). This waiver does not extend to any page images or other supplementary files associated with this work, which may be protected by copyright or other license restrictions. Please go to http://www.textcreationpartnership.org/ for more information.</p>
    </availability>
   </publicationStmt>
   <seriesStmt>
    <title>Early English books online.</title>
   </seriesStmt>
   <notesStmt>
    <note>(EEBO-TCP ; phase 1, no. A21569)</note>
    <note>Transcribed from: (Early English Books Online ; image set 28771)</note>
    <note>Images scanned from microfilm: (Early English books, 1475-1640 ; 1874:47)</note>
   </notesStmt>
   <sourceDesc>
    <biblFull>
     <titleStmt>
      <title type="_245">By the Kyng and the Quene the King and Quenes maiesties being credible enfourmed that diuerse and many there louing faythfull and obedient subiectes, inhabityng vpon the sea costes, and vsyng traffyque by sea, and dyuers others be very desirous to prepare and esquippe sundry shypes ... for the anoyaunce of there maiesties enemies the  Frenchmen ...</title>
      <author>England and Wales. Sovereign (1553-1558 : Mary I)</author>
      <author>Mary I, Queen of England, 1516-1558.</author>
     </titleStmt>
     <extent>1 sheet ([1] p.).   </extent>
     <publicationStmt>
      <publisher>In ædibus Iohannis Cawodi Tipographi Regiæ Maiestatis,</publisher>
      <pubPlace>Excusum Londini :</pubPlace>
      <date>[1557]</date>
     </publicationStmt>
     <notesStmt>
      <note>Contains woodcut initial showing Neptune with two seahorses.</note>
      <note>Other title information from first six lines of text.</note>
      <note>Date of publication supplied from STC (2nd ed.).</note>
      <note>Geuen at our pallaice of Westminster the ix. daye of June the third and fourth yeares of our Reygnes."</note>
      <note>"Cum priuilegio ad imprimendum solum."</note>
      <note>Reproduction of original in: Society of Antiquaries.</note>
     </notesStmt>
    </biblFull>
   </sourceDesc>
  </fileDesc>
  <encodingDesc>
   <projectDesc>
    <p>Header created with script mrcb2eeboutf.xsl on 2014-03-05.</p>
   </projectDesc>
   <editorialDecl n="4">
    <p>Manually keyed and coded text linked to page images in accordance with level 4 of the TEI in Libraries guidelines.</p>
    <p>Issued variously as SGML (TCP schema; ASCII text with mnemonic sdata character entities); displayable XML (TCP schema; characters represented either as UTF-8 Unicode or text strings within braces); or lossless XML (TEI P5, characters represented either as UTF-8 Unicode or TEI g elements).</p>
    <p>Keying and markup guidelines available at TCP web site (http://www.textcreationpartnership.org/docs/)</p>
   </editorialDecl>
  </encodingDesc>
  <profileDesc>
   <langUsage>
    <language ident="eng">eng</language>
   </langUsage>
   <textClass>
    <keywords scheme="lcsh">
     <term>Privateering --  England --  Early works to 1800.</term>
     <term>Great Britain --  History --  Mary I, 1553-1558.</term>
     <term>Great Britain --  Foreign relations --  France.</term>
     <term>France --  Foreign relations --  Great Britain.</term>
     <term>Broadsides --  London (England) --  16th century.</term>
    </keywords>
   </textClass>
  </profileDesc>
  <xenoData>
   <ep:epHeader xmlns:ep="http://earlyprint.org/ns/1.0">
    <ep:corpus>eebo</ep:corpus>
    <ep:title>By the Kyng and the Quene the King and Quenes maiesties being credible enfourmed that diuerse and many there louing faythfull and obedient subiectes,</ep:title>
    <ep:author>England and Wales. Sovereign </ep:author>
    <ep:publicationYear>1557</ep:publicationYear>
    <ep:creationYear/>
    <ep:curator>
     <ep:name/>
    </ep:curator>
    <ep:pageCount>1</ep:pageCount>
    <ep:wordCount>355</ep:wordCount>
    <ep:defectiveTokenCount>0</ep:defectiveTokenCount>
    <ep:untranscribedForeignCount>0</ep:untranscribedForeignCount>
    <ep:untranscribedMathCount>0</ep:untranscribedMathCount>
    <ep:untranscribedMusicCount>0</ep:untranscribedMusicCount>
    <ep:missingChunkCount>0</ep:missingChunkCount>
    <ep:missingPagesCount>0</ep:missingPagesCount>
    <ep:defectRate>0</ep:defectRate>
    <ep:finalGrade>A</ep:finalGrade>
    <ep:defectRangePerGrade>This text  has no known defects that were recorded as gap elements at the time of transcription. </ep:defectRangePerGrade>
   </ep:epHeader>
  </xenoData>
  <revisionDesc>
   <change>
    <date>2007-08</date>
    <label>TCP</label>Assigned for keying and markup
      </change>
   <change>
    <date>2007-09</date>
    <label>Apex CoVantage</label>Keyed and coded from ProQuest page images
      </change>
   <change>
    <date>2007-10</date>
    <label>Elspeth Healey</label>Sampled and proofread
      </change>
   <change>
    <date>2007-10</date>
    <label>Elspeth Healey</label>Text and markup reviewed and edited
      </change>
   <change>
    <date>2008-02</date>
    <label>pfs</label>Batch review (QC) and XML conversion
      </change>
  </revisionDesc>
 </teiHeader>
 <text xml:id="A21569-e10010" xml:lang="eng">
  <body xml:id="A21569-e10020">
   <div type="proclamation" xml:id="A21569-e10030">
    <pb facs="tcp:28771:1" xml:id="A21569-001-a"/>
    <head xml:id="A21569-e10040">
     <w lemma="by" pos="acp" xml:id="A21569-001-a-0010">By</w>
     <w lemma="the" pos="d" xml:id="A21569-001-a-0020">the</w>
     <w lemma="king" pos="n1" reg="king" xml:id="A21569-001-a-0030">Kynge</w>
     <w lemma="and" pos="cc" xml:id="A21569-001-a-0040">and</w>
     <w lemma="the" pos="d" xml:id="A21569-001-a-0050">the</w>
     <w lemma="queen" pos="n1" reg="queen" xml:id="A21569-001-a-0060">Quene</w>
     <pc unit="sentence" xml:id="A21569-001-a-0070">.</pc>
    </head>
    <p xml:id="A21569-e10050">
     <w lemma="where" pos="crq" rend="decorinit" xml:id="A21569-001-a-0080">WHere</w>
     <w lemma="at" pos="acp" xml:id="A21569-001-a-0090">at</w>
     <w lemma="the" pos="d" xml:id="A21569-001-a-0100">the</w>
     <w lemma="open" pos="vvg" reg="opening" xml:id="A21569-001-a-0110">openyng</w>
     <w lemma="and" pos="cc" xml:id="A21569-001-a-0120">and</w>
     <w lemma="begin" pos="vvg" reg="beginning" xml:id="A21569-001-a-0130">begynnyng</w>
     <w lemma="of" pos="acp" xml:id="A21569-001-a-0140">of</w>
     <w lemma="the" pos="d" xml:id="A21569-001-a-0150">the</w>
     <w lemma="war" pos="n2" xml:id="A21569-001-a-0160">warres</w>
     <w lemma="with" pos="acp" xml:id="A21569-001-a-0170">with</w>
     <w lemma="the" pos="d" xml:id="A21569-001-a-0180">the</w>
     <w lemma="french" pos="jnn" reg="french" xml:id="A21569-001-a-0190">frenche</w>
     <pc xml:id="A21569-001-a-0200">,</pc>
     <w lemma="the" pos="d" xml:id="A21569-001-a-0210">the</w>
     <w lemma="queen" pos="ng1" reg="queens" xml:id="A21569-001-a-0220">Queenes</w>
     <w lemma="majesty" pos="n1" reg="majesty" xml:id="A21569-001-a-0230">Maiestie</w>
     <w lemma="cause" pos="vvd" xml:id="A21569-001-a-0240">caused</w>
     <w lemma="her" pos="po" xml:id="A21569-001-a-0250">her</w>
     <w lemma="proclamation" pos="n1" reg="proclamation" xml:id="A21569-001-a-0260">proclamacyon</w>
     <w lemma="to" pos="prt" xml:id="A21569-001-a-0270">to</w>
     <w lemma="be" pos="vvi" xml:id="A21569-001-a-0280">be</w>
     <w lemma="make" pos="vvn" xml:id="A21569-001-a-0290">made</w>
     <w lemma="and" pos="cc" xml:id="A21569-001-a-0300">and</w>
     <w lemma="therein" pos="av" xml:id="A21569-001-a-0310">therein</w>
     <w lemma="give" pos="vvd" reg="gave" xml:id="A21569-001-a-0320">gaue</w>
     <w lemma="warning" pos="n1" xml:id="A21569-001-a-0330">warning</w>
     <w lemma="and" pos="cc" xml:id="A21569-001-a-0340">and</w>
     <w lemma="licence" pos="n1" xml:id="A21569-001-a-0350">licence</w>
     <w lemma="to" pos="acp" xml:id="A21569-001-a-0360">to</w>
     <w lemma="so" pos="av" xml:id="A21569-001-a-0370">so</w>
     <w lemma="many" pos="d" xml:id="A21569-001-a-0380">many</w>
     <w lemma="of" pos="acp" xml:id="A21569-001-a-0390">of</w>
     <w lemma="that" pos="d" xml:id="A21569-001-a-0400">that</w>
     <w lemma="nation" pos="n1" xml:id="A21569-001-a-0410">nation</w>
     <w lemma="inhabit" pos="vvg" xml:id="A21569-001-a-0420">inhabitynge</w>
     <w lemma="within" pos="acp" xml:id="A21569-001-a-0430">within</w>
     <w lemma="this" pos="d" xml:id="A21569-001-a-0440">this</w>
     <w lemma="realm" pos="n1" reg="realm" xml:id="A21569-001-a-0450">Realme</w>
     <w lemma="as" pos="acp" xml:id="A21569-001-a-0460">as</w>
     <w lemma="be" pos="vvd" xml:id="A21569-001-a-0470">were</w>
     <w lemma="not" pos="xx" xml:id="A21569-001-a-0480">not</w>
     <w lemma="denizen" pos="n1" reg="denizens" xml:id="A21569-001-a-0490">denizons</w>
     <w lemma="to" pos="prt" xml:id="A21569-001-a-0500">to</w>
     <w lemma="depart" pos="vvi" xml:id="A21569-001-a-0510">depart</w>
     <w lemma="the" pos="d" xml:id="A21569-001-a-0520">the</w>
     <w lemma="same" pos="d" xml:id="A21569-001-a-0530">same</w>
     <pc xml:id="A21569-001-a-0540">,</pc>
     <w lemma="within" pos="acp" xml:id="A21569-001-a-0550">within</w>
     <w lemma="forty" pos="crd" xml:id="A21569-001-a-0560">forty</w>
     <w lemma="day" pos="n2" xml:id="A21569-001-a-0570">daies</w>
     <w lemma="after" pos="acp" xml:id="A21569-001-a-0580">after</w>
     <pc xml:id="A21569-001-a-0590">,</pc>
     <w lemma="upon" pos="acp" xml:id="A21569-001-a-0600">vpon</w>
     <w lemma="pain" pos="n1" reg="pain" xml:id="A21569-001-a-0610">payne</w>
     <w lemma="to" pos="prt" xml:id="A21569-001-a-0620">to</w>
     <w lemma="be" pos="vvi" xml:id="A21569-001-a-0630">be</w>
     <w lemma="take" pos="vvn" xml:id="A21569-001-a-0640">taken</w>
     <w lemma="as" pos="acp" xml:id="A21569-001-a-0650">as</w>
     <w lemma="enemy" pos="n2" xml:id="A21569-001-a-0660">enemies</w>
     <pc unit="sentence" xml:id="A21569-001-a-0670">.</pc>
     <w lemma="for" pos="acp" xml:id="A21569-001-a-0680">For</w>
     <w lemma="asmuch" pos="av" reg="as much" xml:id="A21569-001-a-0690">asmuch</w>
     <w lemma="as" pos="acp" xml:id="A21569-001-a-0700">as</w>
     <w lemma="her" pos="po" xml:id="A21569-001-a-0710">her</w>
     <w lemma="highness" pos="n1" xml:id="A21569-001-a-0720">highnes</w>
     <w lemma="be" pos="vvz" xml:id="A21569-001-a-0730">is</w>
     <w lemma="give" pos="vvn" reg="given" xml:id="A21569-001-a-0740">giuen</w>
     <w lemma="to" pos="prt" xml:id="A21569-001-a-0750">to</w>
     <w lemma="understand" pos="vvi" reg="understand" xml:id="A21569-001-a-0760">vnderstand</w>
     <w lemma="that" pos="cs" xml:id="A21569-001-a-0770">that</w>
     <w lemma="this" pos="d" xml:id="A21569-001-a-0780">this</w>
     <w lemma="notwithstanding" pos="acp" xml:id="A21569-001-a-0790">notwithstanding</w>
     <w lemma="there" pos="av" xml:id="A21569-001-a-0800">there</w>
     <w lemma="remain" pos="vvz" reg="remaineth" xml:id="A21569-001-a-0810">remayneth</w>
     <w lemma="to" pos="acp" xml:id="A21569-001-a-0820">to</w>
     <w lemma="this" pos="d" xml:id="A21569-001-a-0830">this</w>
     <w lemma="hour" pos="n1" reg="hour" xml:id="A21569-001-a-0840">houre</w>
     <pc xml:id="A21569-001-a-0850">,</pc>
     <w lemma="aswell" pos="av" reg="as well" xml:id="A21569-001-a-0860">aswell</w>
     <w lemma="within" pos="acp" xml:id="A21569-001-a-0870">within</w>
     <w lemma="the" pos="d" xml:id="A21569-001-a-0880">the</w>
     <w lemma="city" pos="n1" reg="city" xml:id="A21569-001-a-0890">Citie</w>
     <w lemma="of" pos="acp" xml:id="A21569-001-a-0900">of</w>
     <w lemma="london" pos="nn1" xml:id="A21569-001-a-0910">London</w>
     <pc xml:id="A21569-001-a-0920">,</pc>
     <w lemma="as" pos="acp" xml:id="A21569-001-a-0930">as</w>
     <w lemma="else" pos="av" xml:id="A21569-001-a-0940">ells</w>
     <w lemma="where" pos="crq" xml:id="A21569-001-a-0950">where</w>
     <w lemma="in" pos="acp" xml:id="A21569-001-a-0960">in</w>
     <w lemma="sundry" pos="j" reg="sundry" xml:id="A21569-001-a-0970">sundrye</w>
     <w lemma="other" pos="d" xml:id="A21569-001-a-0980">other</w>
     <w lemma="part" pos="n2" xml:id="A21569-001-a-0990">partes</w>
     <w lemma="of" pos="acp" xml:id="A21569-001-a-1000">of</w>
     <w lemma="the" pos="d" xml:id="A21569-001-a-1010">the</w>
     <w lemma="realm" pos="n1" reg="realm" xml:id="A21569-001-a-1020">Realme</w>
     <pc xml:id="A21569-001-a-1030">,</pc>
     <w lemma="no" pos="dx" xml:id="A21569-001-a-1040">no</w>
     <w lemma="small" pos="j" reg="small" xml:id="A21569-001-a-1050">smale</w>
     <w lemma="number" pos="j-vn" reg="number" xml:id="A21569-001-a-1060">numbre</w>
     <w lemma="of" pos="acp" xml:id="A21569-001-a-1070">of</w>
     <w lemma="the" pos="d" xml:id="A21569-001-a-1080">the</w>
     <w lemma="say" pos="j-vn" reg="said" xml:id="A21569-001-a-1090">saide</w>
     <w lemma="french" pos="jnn" xml:id="A21569-001-a-1100">french</w>
     <w lemma="man" pos="n2" xml:id="A21569-001-a-1110">men</w>
     <w lemma="which" pos="crq" reg="which" xml:id="A21569-001-a-1120">whiche</w>
     <w lemma="be" pos="vvb" xml:id="A21569-001-a-1130">be</w>
     <w lemma="no" pos="dx" xml:id="A21569-001-a-1140">no</w>
     <w lemma="denizen" pos="n1" reg="denizens" xml:id="A21569-001-a-1150">denizons</w>
     <w lemma="at" pos="acp" xml:id="A21569-001-a-1160">at</w>
     <w lemma="all" pos="d" xml:id="A21569-001-a-1170">all</w>
     <pc xml:id="A21569-001-a-1180">,</pc>
     <w lemma="and" pos="cc" xml:id="A21569-001-a-1190">and</w>
     <w lemma="yet" pos="av" xml:id="A21569-001-a-1200">yet</w>
     <w lemma="not" pos="xx" xml:id="A21569-001-a-1210">not</w>
     <w lemma="avoid" pos="vvn" reg="avoyded" xml:id="A21569-001-a-1220">auoyded</w>
     <w lemma="hence" pos="av" xml:id="A21569-001-a-1230">hence</w>
     <w lemma="accord" pos="vvg" reg="according" xml:id="A21569-001-a-1240">accordyng</w>
     <w lemma="to" pos="acp" xml:id="A21569-001-a-1250">to</w>
     <w lemma="the" pos="d" xml:id="A21569-001-a-1260">the</w>
     <w lemma="tenure" pos="n1" reg="tenor" xml:id="A21569-001-a-1270">tenure</w>
     <w lemma="of" pos="acp" xml:id="A21569-001-a-1280">of</w>
     <w lemma="the" pos="d" xml:id="A21569-001-a-1290">the</w>
     <w lemma="say" pos="j-vn" reg="said" xml:id="A21569-001-a-1300">sayde</w>
     <w lemma="proclamation" pos="n1" xml:id="A21569-001-a-1310">proclamation</w>
     <pc xml:id="A21569-001-a-1320">,</pc>
     <w lemma="to" pos="acp" xml:id="A21569-001-a-1330">to</w>
     <w lemma="the" pos="d" xml:id="A21569-001-a-1340">the</w>
     <w lemma="manifest" pos="j" reg="manifest" xml:id="A21569-001-a-1350">manyfest</w>
     <w lemma="contempt" pos="n1" xml:id="A21569-001-a-1360">contempt</w>
     <w lemma="thereof" pos="av" xml:id="A21569-001-a-1370">thereof</w>
     <pc xml:id="A21569-001-a-1380">,</pc>
     <w lemma="and" pos="cc" xml:id="A21569-001-a-1390">and</w>
     <w lemma="tend" pos="vvg" reg="tending" xml:id="A21569-001-a-1400">tendyng</w>
     <w lemma="not" pos="xx" xml:id="A21569-001-a-1410">not</w>
     <w lemma="only" pos="av-j" xml:id="A21569-001-a-1420">onely</w>
     <w lemma="to" pos="acp" xml:id="A21569-001-a-1430">to</w>
     <w lemma="the" pos="d" xml:id="A21569-001-a-1440">the</w>
     <w lemma="great" pos="j" xml:id="A21569-001-a-1450">great</w>
     <w lemma="danger" pos="n1" reg="danger" xml:id="A21569-001-a-1460">daunger</w>
     <w lemma="of" pos="acp" xml:id="A21569-001-a-1470">of</w>
     <w lemma="this" pos="d" reg="this" xml:id="A21569-001-a-1480">thys</w>
     <w lemma="common" pos="j" xml:id="A21569-001-a-1490">common</w>
     <w lemma="wealth" pos="n1" reg="wealth" xml:id="A21569-001-a-1500">welth</w>
     <pc xml:id="A21569-001-a-1510">,</pc>
     <w lemma="by" pos="acp" xml:id="A21569-001-a-1520">by</w>
     <w lemma="such" pos="d" xml:id="A21569-001-a-1530">such</w>
     <w lemma="practise" pos="n2" xml:id="A21569-001-a-1540">practyses</w>
     <w lemma="as" pos="acp" xml:id="A21569-001-a-1550">as</w>
     <w lemma="they" pos="pns" xml:id="A21569-001-a-1560">they</w>
     <w lemma="may" pos="vmd" reg="might" xml:id="A21569-001-a-1570">myghte</w>
     <w lemma="attempt" pos="vvi" reg="attempt" xml:id="A21569-001-a-1580">attempte</w>
     <pc unit="sentence" xml:id="A21569-001-a-1590">.</pc>
     <w lemma="but" pos="acp" xml:id="A21569-001-a-1600">But</w>
     <w lemma="also" pos="av" xml:id="A21569-001-a-1610">also</w>
     <w lemma="minister" pos="j-vg" reg="ministering" xml:id="A21569-001-a-1620">ministryng</w>
     <w lemma="just" pos="j" reg="just" xml:id="A21569-001-a-1630">iust</w>
     <w lemma="occasion" pos="n1" xml:id="A21569-001-a-1640">occasion</w>
     <w lemma="of" pos="acp" xml:id="A21569-001-a-1650">of</w>
     <w lemma="murmur" pos="n1" xml:id="A21569-001-a-1660">murmour</w>
     <pc xml:id="A21569-001-a-1670">,</pc>
     <w lemma="and" pos="cc" xml:id="A21569-001-a-1680">and</w>
     <w lemma="discontentation" pos="n1" xml:id="A21569-001-a-1690">discontentacion</w>
     <w lemma="to" pos="acp" xml:id="A21569-001-a-1700">to</w>
     <w lemma="other" pos="pi2-d" xml:id="A21569-001-a-1710">others</w>
     <w lemma="her" pos="po" xml:id="A21569-001-a-1720">her</w>
     <w lemma="natural" pos="j" reg="natural" xml:id="A21569-001-a-1730">naturall</w>
     <w lemma="and" pos="cc" xml:id="A21569-001-a-1740">and</w>
     <w lemma="love" pos="j-vg" reg="loving" xml:id="A21569-001-a-1750">louyng</w>
     <w lemma="subject" pos="n2-j" xml:id="A21569-001-a-1760">subiectes</w>
     <pc xml:id="A21569-001-a-1770">:</pc>
     <w lemma="her" pos="po" xml:id="A21569-001-a-1780">Her</w>
     <w lemma="majesty" pos="ng1" reg="majesties" xml:id="A21569-001-a-1790">Maiesties</w>
     <w lemma="pleasure" pos="n1" xml:id="A21569-001-a-1800">pleasure</w>
     <w lemma="therefore" pos="av" xml:id="A21569-001-a-1810">therefore</w>
     <w lemma="be" pos="vvz" xml:id="A21569-001-a-1820">is</w>
     <pc xml:id="A21569-001-a-1830">,</pc>
     <w lemma="forasmuch" pos="av" xml:id="A21569-001-a-1840">forasmuch</w>
     <w lemma="as" pos="acp" xml:id="A21569-001-a-1850">as</w>
     <w lemma="they" pos="pns" xml:id="A21569-001-a-1860">they</w>
     <w lemma="have" pos="vvb" xml:id="A21569-001-a-1870">haue</w>
     <w lemma="not" pos="xx" xml:id="A21569-001-a-1880">not</w>
     <w lemma="use" pos="vvn" reg="used" xml:id="A21569-001-a-1890">vsed</w>
     <w lemma="her" pos="po" xml:id="A21569-001-a-1900">her</w>
     <w lemma="highness" pos="n1" reg="highness" xml:id="A21569-001-a-1910">hyghnes</w>
     <w lemma="clemency" pos="n1" reg="clemency" xml:id="A21569-001-a-1920">clemencye</w>
     <w lemma="and" pos="cc" xml:id="A21569-001-a-1930">and</w>
     <w lemma="mercy" pos="n1" reg="mercy" xml:id="A21569-001-a-1940">mercye</w>
     <pc xml:id="A21569-001-a-1950">,</pc>
     <w lemma="but" pos="acp" xml:id="A21569-001-a-1960">but</w>
     <w lemma="contrary" pos="j" xml:id="A21569-001-a-1970">contrary</w>
     <w lemma="wise" pos="n1" reg="wise" xml:id="A21569-001-a-1980">wyse</w>
     <w lemma="by" pos="acp" xml:id="A21569-001-a-1990">by</w>
     <w lemma="disobey" pos="vvg" xml:id="A21569-001-a-2000">disobeing</w>
     <w lemma="the" pos="d" xml:id="A21569-001-a-2010">the</w>
     <w lemma="same" pos="d" xml:id="A21569-001-a-2020">same</w>
     <w lemma="show" pos="vvd" xml:id="A21569-001-a-2030">shewed</w>
     <w lemma="a" pos="d" xml:id="A21569-001-a-2040">a</w>
     <w lemma="manifest" pos="j" xml:id="A21569-001-a-2050">manifest</w>
     <w lemma="contempt" pos="n1" reg="contempt" xml:id="A21569-001-a-2060">contempte</w>
     <w lemma="of" pos="acp" xml:id="A21569-001-a-2070">of</w>
     <w lemma="her" pos="po" xml:id="A21569-001-a-2080">her</w>
     <w lemma="commandment" pos="n1" reg="commandment" xml:id="A21569-001-a-2090">commaundemente</w>
     <pc xml:id="A21569-001-a-2100">,</pc>
     <w lemma="that" pos="cs" xml:id="A21569-001-a-2110">that</w>
     <w lemma="forthwith" orig="furthwᵗ" pos="av" xml:id="A21569-001-a-2120">furthwt</w>
     <w lemma="upon" pos="acp" xml:id="A21569-001-a-2140">vpon</w>
     <w lemma="the" pos="d" xml:id="A21569-001-a-2150">the</w>
     <w lemma="publish" pos="vvg" xml:id="A21569-001-a-2160">publyshyng</w>
     <w lemma="of" pos="acp" xml:id="A21569-001-a-2170">of</w>
     <w lemma="this" pos="d" reg="this" xml:id="A21569-001-a-2180">thys</w>
     <w lemma="her" pos="po" xml:id="A21569-001-a-2190">her</w>
     <w lemma="highness" pos="n1" xml:id="A21569-001-a-2200">highnes</w>
     <w lemma="present" pos="j" reg="present" xml:id="A21569-001-a-2210">presente</w>
     <w lemma="proclamation" pos="n1" xml:id="A21569-001-a-2220">proclamation</w>
     <pc xml:id="A21569-001-a-2230">,</pc>
     <w lemma="it" pos="pn" xml:id="A21569-001-a-2240">it</w>
     <w join="right" lemma="shall" pos="vmb" xml:id="A21569-001-a-2250">shal</w>
     <w join="left" lemma="be" pos="vvb" xml:id="A21569-001-a-2251">be</w>
     <w lemma="leful" pos="j" reg="leeful" xml:id="A21569-001-a-2260">leful</w>
     <w lemma="for" pos="acp" xml:id="A21569-001-a-2270">for</w>
     <w lemma="any" pos="d" reg="any" xml:id="A21569-001-a-2280">anye</w>
     <w lemma="of" pos="acp" xml:id="A21569-001-a-2290">of</w>
     <w lemma="her" pos="po" xml:id="A21569-001-a-2300">her</w>
     <w lemma="say" pos="j-vn" reg="said" xml:id="A21569-001-a-2310">sayde</w>
     <w lemma="love" pos="j-vg" reg="loving" xml:id="A21569-001-a-2320">louynge</w>
     <w lemma="subject" pos="n2-j" xml:id="A21569-001-a-2330">subiectes</w>
     <w lemma="not" pos="xx" xml:id="A21569-001-a-2340">not</w>
     <w lemma="only" pos="j" xml:id="A21569-001-a-2350">onely</w>
     <w lemma="to" pos="prt" xml:id="A21569-001-a-2360">to</w>
     <w lemma="take" pos="vvi" xml:id="A21569-001-a-2370">take</w>
     <w lemma="the" pos="d" xml:id="A21569-001-a-2380">the</w>
     <w lemma="say" pos="j-vn" reg="said" xml:id="A21569-001-a-2390">sayd</w>
     <w lemma="french" pos="jnn" xml:id="A21569-001-a-2400">french</w>
     <w lemma="man" pos="n2" xml:id="A21569-001-a-2410">men</w>
     <w lemma="not" pos="xx" xml:id="A21569-001-a-2420">not</w>
     <w lemma="be" pos="vvg" xml:id="A21569-001-a-2430">being</w>
     <w lemma="denyzon" pos="n2" xml:id="A21569-001-a-2440">denyzons</w>
     <pc xml:id="A21569-001-a-2450">,</pc>
     <w lemma="and" pos="cc" xml:id="A21569-001-a-2460">and</w>
     <w lemma="every" pos="d" reg="every" xml:id="A21569-001-a-2470">euerye</w>
     <w lemma="of" pos="acp" xml:id="A21569-001-a-2480">of</w>
     <w lemma="they" pos="pno" xml:id="A21569-001-a-2490">thē</w>
     <w lemma="prisoner" pos="n2" xml:id="A21569-001-a-2500">prisoners</w>
     <pc xml:id="A21569-001-a-2510">,</pc>
     <w lemma="and" pos="cc" xml:id="A21569-001-a-2520">and</w>
     <w lemma="so" pos="av" xml:id="A21569-001-a-2530">so</w>
     <w lemma="to" pos="prt" xml:id="A21569-001-a-2540">to</w>
     <w lemma="use" pos="vvi" reg="use" xml:id="A21569-001-a-2550">vse</w>
     <w lemma="they" pos="pno" xml:id="A21569-001-a-2560">them</w>
     <pc xml:id="A21569-001-a-2570">,</pc>
     <w lemma="but" pos="acp" xml:id="A21569-001-a-2580">But</w>
     <w lemma="also" pos="av" xml:id="A21569-001-a-2590">also</w>
     <w lemma="for" pos="acp" xml:id="A21569-001-a-2600">for</w>
     <w lemma="their" pos="po" reg="their" xml:id="A21569-001-a-2610">theyr</w>
     <w lemma="apprehension" pos="n1" xml:id="A21569-001-a-2620">apprehensyon</w>
     <w lemma="and" pos="cc" xml:id="A21569-001-a-2630">and</w>
     <w lemma="take" pos="vvg" reg="taking" xml:id="A21569-001-a-2640">takyng</w>
     <w lemma="of" pos="acp" xml:id="A21569-001-a-2650">of</w>
     <w lemma="they" pos="pno" xml:id="A21569-001-a-2660">them</w>
     <w lemma="in" pos="acp" xml:id="A21569-001-a-2670">in</w>
     <w lemma="that" pos="d" xml:id="A21569-001-a-2680">that</w>
     <w lemma="sort" pos="n1" reg="sort" xml:id="A21569-001-a-2690">sorte</w>
     <w lemma="shall" pos="vmb" xml:id="A21569-001-a-2700">shall</w>
     <w lemma="enjoy" pos="vvi" reg="enjoy" xml:id="A21569-001-a-2710">enioye</w>
     <w lemma="to" pos="acp" xml:id="A21569-001-a-2720">to</w>
     <w lemma="their" pos="po" xml:id="A21569-001-a-2730">their</w>
     <w lemma="own" pos="d" reg="own" xml:id="A21569-001-a-2740">owne</w>
     <w lemma="proper" pos="j" xml:id="A21569-001-a-2750">proper</w>
     <w lemma="use" pos="n1" reg="use" xml:id="A21569-001-a-2760">vse</w>
     <w lemma="all" pos="d" xml:id="A21569-001-a-2770">all</w>
     <w lemma="such" pos="d" xml:id="A21569-001-a-2780">such</w>
     <w lemma="good" pos="n2-j" reg="goods" xml:id="A21569-001-a-2790">goodes</w>
     <w lemma="and" pos="cc" xml:id="A21569-001-a-2800">and</w>
     <w lemma="cattelle" pos="n2" xml:id="A21569-001-a-2810">cattelles</w>
     <pc xml:id="A21569-001-a-2820">,</pc>
     <w lemma="as" pos="acp" xml:id="A21569-001-a-2830">as</w>
     <w lemma="the" pos="d" xml:id="A21569-001-a-2840">the</w>
     <w lemma="say" pos="j-vn" reg="said" xml:id="A21569-001-a-2850">sayde</w>
     <w lemma="french" pos="jnn" xml:id="A21569-001-a-2860">french</w>
     <w lemma="or" pos="cc" xml:id="A21569-001-a-2870">or</w>
     <w lemma="any" pos="d" xml:id="A21569-001-a-2880">any</w>
     <w lemma="of" pos="acp" xml:id="A21569-001-a-2890">of</w>
     <w lemma="they" pos="pno" xml:id="A21569-001-a-2900">them</w>
     <w lemma="have" pos="vvd" xml:id="A21569-001-a-2910">had</w>
     <pc xml:id="A21569-001-a-2920">,</pc>
     <w lemma="or" pos="cc" xml:id="A21569-001-a-2930">or</w>
     <w lemma="possess" pos="vvn" xml:id="A21569-001-a-2940">possessed</w>
     <w lemma="at" pos="acp" xml:id="A21569-001-a-2950">at</w>
     <w lemma="the" pos="d" xml:id="A21569-001-a-2960">the</w>
     <w lemma="time" pos="n1" reg="time" xml:id="A21569-001-a-2970">tyme</w>
     <w lemma="they" pos="pns" xml:id="A21569-001-a-2980">they</w>
     <w lemma="be" pos="vvd" xml:id="A21569-001-a-2990">were</w>
     <w lemma="take" pos="vvn" xml:id="A21569-001-a-3000">taken</w>
     <pc unit="sentence" xml:id="A21569-001-a-3010">.</pc>
    </p>
    <closer xml:id="A21569-e10060">
     <dateline xml:id="A21569-e10070">
      <w lemma="given" pos="j-vn" reg="given" xml:id="A21569-001-a-3020">Geuen</w>
      <w lemma="at" pos="acp" xml:id="A21569-001-a-3030">at</w>
      <w lemma="westminster" pos="nn1" xml:id="A21569-001-a-3040">Westminster</w>
      <date xml:id="A21569-e10080">
       <w lemma="the" pos="d" xml:id="A21569-001-a-3050">the</w>
       <w lemma="xxvii" pos="crd" xml:id="A21569-001-a-3060">.xxvii.</w>
       <w lemma="day" pos="n1" xml:id="A21569-001-a-3070">day</w>
       <w lemma="of" pos="acp" xml:id="A21569-001-a-3080">of</w>
       <w lemma="january" pos="nn1" reg="january" xml:id="A21569-001-a-3090">Ianuary</w>
       <pc xml:id="A21569-001-a-3100">,</pc>
       <w lemma="the" pos="d" xml:id="A21569-001-a-3110">the</w>
       <w lemma="forth" pos="av" xml:id="A21569-001-a-3120">forth</w>
       <w lemma="and" pos="cc" xml:id="A21569-001-a-3130">and</w>
       <w lemma="five" pos="ord" reg="fift" xml:id="A21569-001-a-3140">fyfte</w>
       <w lemma="year" pos="n2" xml:id="A21569-001-a-3150">yeares</w>
       <w lemma="of" pos="acp" xml:id="A21569-001-a-3160">of</w>
       <w lemma="the" pos="d" xml:id="A21569-001-a-3170">the</w>
       <w lemma="king" pos="n1" reg="king" xml:id="A21569-001-a-3180">Kynge</w>
       <w lemma="and" pos="cc" xml:id="A21569-001-a-3190">and</w>
       <w lemma="queen" pos="ng1" reg="queens" xml:id="A21569-001-a-3200">Queenes</w>
       <w lemma="majesty" pos="n2" reg="majesties" xml:id="A21569-001-a-3210">Maiesties</w>
       <w lemma="most" pos="avs-d" xml:id="A21569-001-a-3220">most</w>
       <w lemma="noble" pos="j" xml:id="A21569-001-a-3230">noble</w>
       <w lemma="reign" pos="vvz" xml:id="A21569-001-a-3240">Raygnes</w>
       <pc unit="sentence" xml:id="A21569-001-a-3250">.</pc>
      </date>
     </dateline>
     <lb xml:id="A21569-e10090"/>
     <salute xml:id="A21569-e10100">
      <w lemma="god" pos="nn1" xml:id="A21569-001-a-3260">God</w>
      <w lemma="save" pos="acp" reg="save" xml:id="A21569-001-a-3270">saue</w>
      <w lemma="the" pos="d" xml:id="A21569-001-a-3280">the</w>
      <w lemma="king" pos="n1" reg="king" xml:id="A21569-001-a-3290">Kynge</w>
      <w lemma="and" pos="cc" xml:id="A21569-001-a-3300">and</w>
      <w lemma="queen" pos="n1" reg="queen" xml:id="A21569-001-a-3310">Queene</w>
      <pc unit="sentence" xml:id="A21569-001-a-3320">.</pc>
     </salute>
    </closer>
   </div>
  </body>
  <back xml:id="A21569-e10110">
   <div type="colophon_and_license" xml:id="A21569-e10120">
    <p xml:id="A21569-e10130">
     <w lemma="❧" pos="sy" xml:id="A21569-001-a-3330">❧</w>
     <hi xml:id="A21569-e10140">
      <w lemma="n/a" pos="fla" reg="excusum" xml:id="A21569-001-a-3340">EXCVSVM</w>
      <w lemma="n/a" pos="fla" xml:id="A21569-001-a-3350">Londini</w>
      <w lemma="in" pos="acp" xml:id="A21569-001-a-3360">in</w>
      <w lemma="n/a" pos="fla" xml:id="A21569-001-a-3370">aedibus</w>
      <w lemma="johannis" pos="nn1" reg="johannis" xml:id="A21569-001-a-3380">Iohannis</w>
      <w lemma="n/a" orig="Cavvodi" pos="fla" xml:id="A21569-001-a-3390">Cawodi</w>
      <pc xml:id="A21569-001-a-3400">,</pc>
      <w lemma="n/a" pos="fla" xml:id="A21569-001-a-3410">Typographi</w>
      <w lemma="n/a" pos="fla" xml:id="A21569-001-a-3420">Regiae</w>
      <w lemma="n/a" pos="fla" xml:id="A21569-001-a-3430">Maiestatis</w>
      <pc unit="sentence" xml:id="A21569-001-a-3440">.</pc>
     </hi>
    </p>
    <p xml:id="A21569-e10150">
     <hi xml:id="A21569-e10160">
      <w lemma="anno." pos="ab" xml:id="A21569-001-a-3450">Anno.</w>
      <w lemma="m." pos="ab" xml:id="A21569-001-a-3460">M.</w>
      <w lemma="d." pos="ab" xml:id="A21569-001-a-3470">D.</w>
      <w lemma="l." pos="ab" xml:id="A21569-001-a-3480">L.</w>
      <w lemma="viii" pos="crd" xml:id="A21569-001-a-3490">viii</w>
      <pc unit="sentence" xml:id="A21569-001-a-3500">.</pc>
      <w lemma="n/a" pos="fla" xml:id="A21569-001-a-3510">Cum</w>
      <w lemma="n/a" pos="fla" reg="privilegio" xml:id="A21569-001-a-3520">priuilegio</w>
      <w lemma="n/a" pos="fla" xml:id="A21569-001-a-3530">Regiae</w>
      <w lemma="n/a" pos="fla" xml:id="A21569-001-a-3540">maiestatis</w>
      <pc unit="sentence" xml:id="A21569-001-a-3550">.</pc>
     </hi>
    </p>
   </div>
  </back>
 </text>
</TEI>
