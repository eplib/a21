<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://schemata.earlyprint.org/schemata/tei_earlyprint.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="A21658">
 <teiHeader>
  <fileDesc>
   <titleStmt>
    <title type="_245">By the Quene the Quenes Maiestie strayghtly co[m]maundeth all maner of her admirals ... to permit &amp; suffer al maner of subiectes of her good brothers the King of Spaine tradyng the seas ...</title>
    <author>England and Wales. Sovereign (1558-1603 : Elizabeth I)</author>
   </titleStmt>
   <editionStmt>
    <edition>This text is an enriched version of the TCP digital transcription A21658 of text S4621 in the <ref target="A21658-http;//estc.bl.uk">English Short Title Catalog</ref>. Textual changes  and metadata enrichments aim at making the text more  computationally tractable, easier to read, and suitable for network-based collaborative curation by amateur and professional end users from many walks of life.  The text has been tokenized and linguistically annotated with <ref target="http://morphadorner.northwestern.edu/morphadorner/">MorphAdorner</ref>. The annotation includes standard spellings that support the display of a text in	 a standardized format that preserves archaic forms ('loveth', 'seekest'). Textual changes aim at restoring the text the author or stationer meant to publish.</edition>
    <respStmt>
     <resp>This text has not been fully proofread</resp>
     <name/>
    </respStmt>
   </editionStmt>
   <extent>Approx. 2 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
   <publicationStmt>
    <publisher>EarlyPrint Project</publisher>
    <pubPlace>Evanston,IL, Notre Dame, IN, St. Louis, MO</pubPlace>
    <date>2018</date>
    <idno type="DLPS">A21658</idno>
    <idno type="stc">STC 7962</idno>
    <idno type="stc">ESTC S4621</idno>
    <idno type="eebo_citation">23664866</idno>
    <idno type="oclc">ocm 23664866</idno>
    <idno type="vid">26783</idno>
    <availability>
     <p>To the extent possible under law, the Text Creation Partnership has waived all copyright and related or neighboring rights to this keyboarded and encoded edition of the work described above, according to the terms of the CC0 1.0 Public Domain Dedication (http://creativecommons.org/publicdomain/zero/1.0/). This waiver does not extend to any page images or other supplementary files associated with this work, which may be protected by copyright or other license restrictions. Please go to http://www.textcreationpartnership.org/ for more information.</p>
    </availability>
   </publicationStmt>
   <seriesStmt>
    <title>Early English books online.</title>
   </seriesStmt>
   <notesStmt>
    <note>(EEBO-TCP ; phase 1, no. A21658)</note>
    <note>Transcribed from: (Early English Books Online ; image set 26783)</note>
    <note>Images scanned from microfilm: (Early English books, 1475-1640 ; 1831:28)</note>
   </notesStmt>
   <sourceDesc>
    <biblFull>
     <titleStmt>
      <title type="_245">By the Quene the Quenes Maiestie strayghtly co[m]maundeth all maner of her admirals ... to permit &amp; suffer al maner of subiectes of her good brothers the King of Spaine tradyng the seas ...</title>
      <author>England and Wales. Sovereign (1558-1603 : Elizabeth I)</author>
      <author>Elizabeth I, Queen of England, 1533-1603.</author>
     </titleStmt>
     <extent>1 broadside.   </extent>
     <publicationStmt>
      <publisher>Imprinted at London in Powles Church yarde, by Richard Jugge and John Cawood ...,</publisher>
      <pubPlace>[London] :</pubPlace>
      <date>[1563]</date>
     </publicationStmt>
     <notesStmt>
      <note>Second pt. of title from first six lines of text.</note>
      <note>"Yeuen at her hyghnes castell of Wyndsor the fyrst day of September, the fyfth yere of her Maiesties reigne."</note>
      <note>"Cum priuilegio Regiæ Maiestatis."</note>
      <note>Date of publication suggested by STC (2nd ed.).</note>
      <note>Reproduction of original in the Queen's College (University of Oxford). Library.</note>
     </notesStmt>
    </biblFull>
   </sourceDesc>
  </fileDesc>
  <encodingDesc>
   <projectDesc>
    <p>Header created with script mrcb2eeboutf.xsl on 2014-03-05.</p>
   </projectDesc>
   <editorialDecl n="4">
    <p>Manually keyed and coded text linked to page images in accordance with level 4 of the TEI in Libraries guidelines.</p>
    <p>Issued variously as SGML (TCP schema; ASCII text with mnemonic sdata character entities); displayable XML (TCP schema; characters represented either as UTF-8 Unicode or text strings within braces); or lossless XML (TEI P5, characters represented either as UTF-8 Unicode or TEI g elements).</p>
    <p>Keying and markup guidelines available at TCP web site (http://www.textcreationpartnership.org/docs/)</p>
   </editorialDecl>
  </encodingDesc>
  <profileDesc>
   <langUsage>
    <language ident="eng">eng</language>
   </langUsage>
   <textClass>
    <keywords scheme="lcsh">
     <term>Proclamations --  Great Britain.</term>
     <term>Great Britain --  History --  Elizabeth, 1558-1603.</term>
     <term>Great Britain --  Foreign relations --  Spain --  1558-1603.</term>
     <term>Spain --  Foreign relations --  Great Britain --  1556-1598.</term>
    </keywords>
   </textClass>
  </profileDesc>
  <xenoData>
   <ep:epHeader xmlns:ep="http://earlyprint.org/ns/1.0">
    <ep:corpus>eebo</ep:corpus>
    <ep:title>By the Quene the Quenes Maiestie strayghtly co[m]maundeth all maner of her admirals ... to permit &amp; suffer al maner of subiectes of her good brothers</ep:title>
    <ep:author>England and Wales. Sovereign (1558-1603 : Elizabeth I)</ep:author>
    <ep:publicationYear>1563</ep:publicationYear>
    <ep:creationYear/>
    <ep:curator>
     <ep:name/>
    </ep:curator>
    <ep:pageCount>1</ep:pageCount>
    <ep:wordCount>314</ep:wordCount>
    <ep:defectiveTokenCount>1</ep:defectiveTokenCount>
    <ep:untranscribedForeignCount>0</ep:untranscribedForeignCount>
    <ep:untranscribedMathCount>0</ep:untranscribedMathCount>
    <ep:untranscribedMusicCount>0</ep:untranscribedMusicCount>
    <ep:missingChunkCount>0</ep:missingChunkCount>
    <ep:missingPagesCount>0</ep:missingPagesCount>
    <ep:defectRate>31.85</ep:defectRate>
    <ep:finalGrade>C</ep:finalGrade>
    <ep:defectRangePerGrade> The  rate of 31.85 defects per 10,000 words puts this text in the C category of texts with between 10 and 35 defects per 10,000 words.</ep:defectRangePerGrade>
   </ep:epHeader>
  </xenoData>
  <revisionDesc>
   <change>
    <date>2002-07</date>
    <label>TCP</label>Assigned for keying and markup
      </change>
   <change>
    <date>2002-09</date>
    <label>SPi Global</label>Keyed and coded from ProQuest page images
      </change>
   <change>
    <date>2002-10</date>
    <label>Chris Scherer</label>Sampled and proofread
      </change>
   <change>
    <date>2002-10</date>
    <label>Chris Scherer</label>Text and markup reviewed and edited
      </change>
   <change>
    <date>2002-12</date>
    <label>pfs</label>Batch review (QC) and XML conversion
      </change>
  </revisionDesc>
 </teiHeader>
 <text xml:id="A21658-e10010" xml:lang="eng">
  <body xml:id="A21658-e10020">
   <div type="text" xml:id="A21658-e10030">
    <pb facs="tcp:26783:1" xml:id="A21658-001-a"/>
    <head xml:id="A21658-e10040">
     <w lemma="by" pos="acp" xml:id="A21658-001-a-0010">By</w>
     <w lemma="the" pos="d" xml:id="A21658-001-a-0020">the</w>
     <w lemma="queen" pos="n1" reg="queen" xml:id="A21658-001-a-0030">Quene</w>
     <pc xml:id="A21658-001-a-0040">,</pc>
    </head>
    <p xml:id="A21658-e10050">
     <w lemma="the" pos="d" rend="decorinit" xml:id="A21658-001-a-0050">THe</w>
     <w lemma="queen" pos="ng1" reg="queens" xml:id="A21658-001-a-0060">Quenes</w>
     <w lemma="majesty" pos="n1" reg="majesty" xml:id="A21658-001-a-0070">Maiestie</w>
     <w lemma="straight" pos="av-j" reg="straightly" xml:id="A21658-001-a-0080">strayghtly</w>
     <w lemma="command" orig="cōmaundeth" pos="vvz" reg="commandeth" xml:id="A21658-001-a-0090">commaundeth</w>
     <w lemma="all" pos="d" xml:id="A21658-001-a-0100">all</w>
     <w lemma="manner" pos="n1" xml:id="A21658-001-a-0110">maner</w>
     <w lemma="her" pos="po" xml:id="A21658-001-a-0120">her</w>
     <w lemma="admiral" pos="n2" xml:id="A21658-001-a-0130">Admirals</w>
     <pc xml:id="A21658-001-a-0140">,</pc>
     <w lemma="uiceadmiral" pos="n2" xml:id="A21658-001-a-0150">Uiceadmirals</w>
     <pc xml:id="A21658-001-a-0160">,</pc>
     <w lemma="captain" pos="n2" reg="captains" xml:id="A21658-001-a-0170">Captaynes</w>
     <pc xml:id="A21658-001-a-0180">,</pc>
     <w lemma="and" pos="cc" xml:id="A21658-001-a-0190">and</w>
     <w lemma="master" pos="n2" xml:id="A21658-001-a-0200">maisters</w>
     <w lemma="of" pos="acp" xml:id="A21658-001-a-0210">of</w>
     <w lemma="her" pos="po" xml:id="A21658-001-a-0220">her</w>
     <w lemma="ship" pos="n2" reg="ships" xml:id="A21658-001-a-0230">Shippes</w>
     <pc xml:id="A21658-001-a-0240">,</pc>
     <w lemma="that" pos="cs" xml:id="A21658-001-a-0250">that</w>
     <w lemma="now" pos="av" reg="now" xml:id="A21658-001-a-0260">nowe</w>
     <w lemma="be" pos="vvb" xml:id="A21658-001-a-0270">be</w>
     <pc xml:id="A21658-001-a-0280">,</pc>
     <w lemma="or" pos="cc" xml:id="A21658-001-a-0290">or</w>
     <w lemma="that" pos="cs" xml:id="A21658-001-a-0300">that</w>
     <w lemma="hereafter" pos="av" xml:id="A21658-001-a-0310">hereafter</w>
     <w lemma="shall" pos="vmb" xml:id="A21658-001-a-0320">shall</w>
     <w lemma="go" pos="vvi" xml:id="A21658-001-a-0330">go</w>
     <w lemma="to" pos="acp" xml:id="A21658-001-a-0340">to</w>
     <w lemma="the" pos="d" xml:id="A21658-001-a-0350">the</w>
     <w lemma="sea" pos="n2" xml:id="A21658-001-a-0360">Seas</w>
     <pc xml:id="A21658-001-a-0370">,</pc>
     <w lemma="and" pos="cc" xml:id="A21658-001-a-0380">and</w>
     <w lemma="likewise" pos="av" reg="likewise" xml:id="A21658-001-a-0390">lykewyse</w>
     <w lemma="all" pos="d" xml:id="A21658-001-a-0400">all</w>
     <w lemma="other" pos="n1-j" xml:id="A21658-001-a-0410">other</w>
     <w lemma="her" pos="po" xml:id="A21658-001-a-0420">her</w>
     <w lemma="subject" pos="n2-j" xml:id="A21658-001-a-0430">Subiectes</w>
     <w lemma="be" pos="vvg" reg="being" xml:id="A21658-001-a-0440">beyng</w>
     <w lemma="ruler" pos="n2" xml:id="A21658-001-a-0450">rulers</w>
     <w lemma="of" pos="acp" xml:id="A21658-001-a-0460">of</w>
     <w lemma="any" pos="d" xml:id="A21658-001-a-0470">any</w>
     <w lemma="ship" pos="n2" reg="ships" xml:id="A21658-001-a-0480">Shippes</w>
     <w lemma="of" pos="acp" xml:id="A21658-001-a-0490">of</w>
     <w lemma="war" pos="n1" reg="war" xml:id="A21658-001-a-0500">warre</w>
     <w lemma="upon" pos="acp" xml:id="A21658-001-a-0510">vpon</w>
     <w lemma="the" pos="d" xml:id="A21658-001-a-0520">the</w>
     <w lemma="sea" pos="n2" xml:id="A21658-001-a-0530">Seas</w>
     <pc xml:id="A21658-001-a-0540">,</pc>
     <w lemma="to" pos="prt" xml:id="A21658-001-a-0550">to</w>
     <w lemma="permit" pos="vvi" xml:id="A21658-001-a-0560">permit</w>
     <w lemma="&amp;" pos="cc" xml:id="A21658-001-a-0570">&amp;</w>
     <w lemma="suffer" pos="vvi" xml:id="A21658-001-a-0580">suffer</w>
     <w lemma="all" pos="d" xml:id="A21658-001-a-0590">al</w>
     <w lemma="manner" pos="n1" xml:id="A21658-001-a-0600">maner</w>
     <w lemma="of" pos="acp" xml:id="A21658-001-a-0610">of</w>
     <w lemma="subject" pos="n2-j" xml:id="A21658-001-a-0620">Subiectes</w>
     <w lemma="of" pos="acp" xml:id="A21658-001-a-0630">of</w>
     <w lemma="her" pos="po" xml:id="A21658-001-a-0640">her</w>
     <w lemma="good" pos="j" xml:id="A21658-001-a-0650">good</w>
     <w lemma="brother" pos="n2" xml:id="A21658-001-a-0660">brothers</w>
     <w lemma="the" pos="d" xml:id="A21658-001-a-0670">the</w>
     <w lemma="king" pos="n1" xml:id="A21658-001-a-0680">king</w>
     <w lemma="of" pos="acp" xml:id="A21658-001-a-0690">of</w>
     <w lemma="spain" pos="nn1" reg="spain" xml:id="A21658-001-a-0700">Spaine</w>
     <w lemma="trade" pos="vvg" reg="trading" xml:id="A21658-001-a-0710">tradyng</w>
     <w lemma="the" pos="d" xml:id="A21658-001-a-0720">the</w>
     <w lemma="sea" pos="n2" xml:id="A21658-001-a-0730">Seas</w>
     <pc xml:id="A21658-001-a-0740">,</pc>
     <w lemma="either" pos="av-d" reg="either" xml:id="A21658-001-a-0750">eyther</w>
     <w lemma="for" pos="acp" xml:id="A21658-001-a-0760">for</w>
     <w lemma="merchandise" pos="n1" reg="merchandise" xml:id="A21658-001-a-0770">marchaundize</w>
     <w lemma="or" pos="cc" xml:id="A21658-001-a-0780">or</w>
     <w lemma="fish" pos="n1-vg" xml:id="A21658-001-a-0790">fyshyng</w>
     <w lemma="of" pos="acp" xml:id="A21658-001-a-0800">of</w>
     <w lemma="herring" pos="n1" reg="herring" xml:id="A21658-001-a-0810">Herryng</w>
     <w lemma="or" pos="cc" xml:id="A21658-001-a-0820">or</w>
     <w lemma="other" pos="d" xml:id="A21658-001-a-0830">other</w>
     <w lemma="fish" pos="n1" reg="fish" xml:id="A21658-001-a-0840">Fyshe</w>
     <pc xml:id="A21658-001-a-0850">,</pc>
     <w lemma="to" pos="prt" xml:id="A21658-001-a-0860">to</w>
     <w lemma="use" pos="vvi" reg="use" xml:id="A21658-001-a-0870">vse</w>
     <w lemma="and" pos="cc" xml:id="A21658-001-a-0880">and</w>
     <w lemma="follow" pos="vvi" reg="follow" xml:id="A21658-001-a-0890">folowe</w>
     <w lemma="the" pos="d" xml:id="A21658-001-a-0900">the</w>
     <w lemma="accustom" pos="j-vn" reg="accustomend" xml:id="A21658-001-a-0910">accustomed</w>
     <w lemma="trade" pos="n1" xml:id="A21658-001-a-0920">trade</w>
     <w lemma="of" pos="acp" xml:id="A21658-001-a-0930">of</w>
     <w lemma="their" pos="po" reg="their" xml:id="A21658-001-a-0940">theyr</w>
     <w lemma="merchandise" pos="n1" reg="merchandise" xml:id="A21658-001-a-0950">marchaundize</w>
     <w lemma="and" pos="cc" xml:id="A21658-001-a-0960">and</w>
     <w lemma="fish" pos="n1-vg" xml:id="A21658-001-a-0970">fyshyng</w>
     <pc xml:id="A21658-001-a-0980">,</pc>
     <w lemma="without" pos="acp" xml:id="A21658-001-a-0990">without</w>
     <w lemma="any" pos="d" xml:id="A21658-001-a-1000">any</w>
     <w lemma="trouble" pos="n1" xml:id="A21658-001-a-1010">trouble</w>
     <pc xml:id="A21658-001-a-1020">,</pc>
     <w lemma="vexation" pos="n1" xml:id="A21658-001-a-1030">vexation</w>
     <pc xml:id="A21658-001-a-1040">,</pc>
     <w lemma="or" pos="cc" xml:id="A21658-001-a-1050">or</w>
     <w lemma="molestation" pos="n1" xml:id="A21658-001-a-1060">molestation</w>
     <pc unit="sentence" xml:id="A21658-001-a-1070">.</pc>
     <w lemma="and" pos="cc" xml:id="A21658-001-a-1080">And</w>
     <w lemma="if" pos="cs" xml:id="A21658-001-a-1090">yf</w>
     <w lemma="any" pos="d" xml:id="A21658-001-a-1100">any</w>
     <w lemma="of" pos="acp" xml:id="A21658-001-a-1110">of</w>
     <w lemma="the" pos="d" xml:id="A21658-001-a-1120">the</w>
     <w lemma="french" pos="jnn" xml:id="A21658-001-a-1130">French</w>
     <w lemma="king" pos="ng1" reg="kings" xml:id="A21658-001-a-1140">kynges</w>
     <w lemma="subject" pos="n2-j" xml:id="A21658-001-a-1150">Subiectes</w>
     <w lemma="haunt" pos="vvg" reg="haunting" xml:id="A21658-001-a-1160">hauntyng</w>
     <w lemma="the" pos="d" xml:id="A21658-001-a-1170">the</w>
     <w lemma="sea" pos="n2" xml:id="A21658-001-a-1180">Seas</w>
     <w lemma="as" pos="acp" xml:id="A21658-001-a-1190">as</w>
     <w lemma="pirate" pos="n2" xml:id="A21658-001-a-1200">Pirates</w>
     <w lemma="or" pos="cc" xml:id="A21658-001-a-1210">or</w>
     <w lemma="man" pos="n2" xml:id="A21658-001-a-1220">men</w>
     <w lemma="of" pos="acp" xml:id="A21658-001-a-1230">of</w>
     <w lemma="war" pos="n1" reg="war" xml:id="A21658-001-a-1240">warre</w>
     <pc xml:id="A21658-001-a-1250">,</pc>
     <w lemma="shall" pos="vmb" xml:id="A21658-001-a-1260">shall</w>
     <pc xml:id="A21658-001-a-1270">(</pc>
     <w lemma="as" pos="acp" xml:id="A21658-001-a-1280">as</w>
     <w lemma="of" pos="acp" xml:id="A21658-001-a-1290">of</w>
     <w lemma="late" pos="av-j" xml:id="A21658-001-a-1300">late</w>
     <w lemma="it" pos="pn" xml:id="A21658-001-a-1310">it</w>
     <w lemma="be" pos="vvz" xml:id="A21658-001-a-1320">is</w>
     <w lemma="understanden" pos="nn1" reg="understanden" xml:id="A21658-001-a-1330">vnderstanden</w>
     <w lemma="they" pos="pns" xml:id="A21658-001-a-1340">they</w>
     <w lemma="have" pos="vvb" xml:id="A21658-001-a-1350">haue</w>
     <pc xml:id="A21658-001-a-1360">)</pc>
     <w lemma="spoil" pos="n1" reg="spoil" xml:id="A21658-001-a-1370">spoyle</w>
     <w lemma="or" pos="cc" xml:id="A21658-001-a-1380">or</w>
     <w lemma="molest" pos="vvi" xml:id="A21658-001-a-1390">molest</w>
     <w lemma="any" pos="d" xml:id="A21658-001-a-1400">any</w>
     <w lemma="of" pos="acp" xml:id="A21658-001-a-1410">of</w>
     <w lemma="the" pos="d" xml:id="A21658-001-a-1420">the</w>
     <w lemma="say" pos="j-vn" reg="said" xml:id="A21658-001-a-1430">sayde</w>
     <w lemma="king" pos="n1" reg="king" xml:id="A21658-001-a-1440">kyng</w>
     <w lemma="of" pos="acp" xml:id="A21658-001-a-1450">of</w>
     <w lemma="spain" pos="nng1" xml:id="A21658-001-a-1460">Spaynes</w>
     <w lemma="subject" pos="n2-j" xml:id="A21658-001-a-1470">Subiectes</w>
     <pc xml:id="A21658-001-a-1480">:</pc>
     <w lemma="her" pos="po" xml:id="A21658-001-a-1490">Her</w>
     <w lemma="majesty" pos="n2" reg="majesties" xml:id="A21658-001-a-1500">Maiesties</w>
     <w lemma="like" pos="acp" reg="like" xml:id="A21658-001-a-1510">lyke</w>
     <w lemma="pleasure" pos="n1" xml:id="A21658-001-a-1520">pleasure</w>
     <w lemma="and" pos="cc" xml:id="A21658-001-a-1530">and</w>
     <w lemma="commandment" pos="n1" reg="commandment" xml:id="A21658-001-a-1540">commaundement</w>
     <w lemma="be" pos="vvz" xml:id="A21658-001-a-1550">is</w>
     <pc xml:id="A21658-001-a-1560">,</pc>
     <w lemma="that" pos="cs" xml:id="A21658-001-a-1570">that</w>
     <w lemma="the" pos="d" xml:id="A21658-001-a-1580">the</w>
     <w lemma="captain" pos="n2" reg="captains" xml:id="A21658-001-a-1590">Captaynes</w>
     <w lemma="as" pos="acp" xml:id="A21658-001-a-1600">as</w>
     <w lemma="well" pos="av" xml:id="A21658-001-a-1610">well</w>
     <w lemma="of" pos="acp" xml:id="A21658-001-a-1620">of</w>
     <w lemma="her" pos="po" xml:id="A21658-001-a-1630">her</w>
     <w lemma="own" pos="d" reg="own" xml:id="A21658-001-a-1640">owne</w>
     <w lemma="proper" pos="j" xml:id="A21658-001-a-1650">proper</w>
     <w lemma="ship" pos="n2" reg="ships" xml:id="A21658-001-a-1660">Shyppes</w>
     <pc xml:id="A21658-001-a-1670">,</pc>
     <w lemma="as" pos="acp" xml:id="A21658-001-a-1680">as</w>
     <w lemma="of" pos="acp" xml:id="A21658-001-a-1690">of</w>
     <w lemma="other" pos="pi2-d" xml:id="A21658-001-a-1700">others</w>
     <pc xml:id="A21658-001-a-1710">,</pc>
     <w lemma="shall" pos="vmb" xml:id="A21658-001-a-1720">shall</w>
     <w lemma="employ" pos="vvi" xml:id="A21658-001-a-1730">employ</w>
     <w lemma="their" pos="po" reg="their" xml:id="A21658-001-a-1740">theyr</w>
     <w lemma="whole" pos="j" xml:id="A21658-001-a-1750">whole</w>
     <w lemma="force" pos="n1" xml:id="A21658-001-a-1760">force</w>
     <pc xml:id="A21658-001-a-1770">,</pc>
     <w lemma="to" pos="prt" xml:id="A21658-001-a-1780">to</w>
     <w lemma="aid" pos="vvi" reg="aid" xml:id="A21658-001-a-1790">ayde</w>
     <pc xml:id="A21658-001-a-1800">,</pc>
     <w lemma="defend" pos="vvi" reg="defend" xml:id="A21658-001-a-1810">defende</w>
     <pc xml:id="A21658-001-a-1820">,</pc>
     <w lemma="and" pos="cc" xml:id="A21658-001-a-1830">and</w>
     <w lemma="rescue" pos="vvi" xml:id="A21658-001-a-1840">rescue</w>
     <w lemma="the" pos="d" xml:id="A21658-001-a-1850">the</w>
     <w lemma="same" pos="d" xml:id="A21658-001-a-1860">same</w>
     <pc xml:id="A21658-001-a-1870">,</pc>
     <w lemma="out" pos="av" xml:id="A21658-001-a-1880">out</w>
     <w lemma="of" pos="acp" xml:id="A21658-001-a-1890">of</w>
     <w lemma="the" pos="d" xml:id="A21658-001-a-1900">the</w>
     <w lemma="hand" pos="n2" reg="hands" xml:id="A21658-001-a-1910">handes</w>
     <w lemma="of" pos="acp" xml:id="A21658-001-a-1920">of</w>
     <w lemma="the" pos="d" xml:id="A21658-001-a-1930">the</w>
     <w lemma="say" pos="j-vn" reg="said" xml:id="A21658-001-a-1940">sayde</w>
     <w lemma="french" pos="jnn" reg="french" xml:id="A21658-001-a-1950">Frenche</w>
     <w lemma="pirate" pos="n2" xml:id="A21658-001-a-1960">Pirates</w>
     <w lemma="or" pos="cc" xml:id="A21658-001-a-1970">or</w>
     <w lemma="man" pos="n2" xml:id="A21658-001-a-1980">men</w>
     <w lemma="of" pos="acp" xml:id="A21658-001-a-1990">of</w>
     <w lemma="war" pos="n1" reg="war" xml:id="A21658-001-a-2000">warre</w>
     <pc xml:id="A21658-001-a-2010">,</pc>
     <w lemma="in" pos="acp" xml:id="A21658-001-a-2020">in</w>
     <w lemma="like" pos="acp" reg="like" xml:id="A21658-001-a-2030">lyke</w>
     <w lemma="manner" pos="n1" xml:id="A21658-001-a-2040">maner</w>
     <w lemma="as" pos="acp" xml:id="A21658-001-a-2050">as</w>
     <w lemma="the" pos="d" xml:id="A21658-001-a-2060">the</w>
     <w lemma="case" pos="n1" xml:id="A21658-001-a-2070">case</w>
     <w lemma="be" pos="vvd" xml:id="A21658-001-a-2080">were</w>
     <w lemma="proper" pos="av-j" xml:id="A21658-001-a-2090">properly</w>
     <w lemma="belong" pos="vvg" reg="belonging" xml:id="A21658-001-a-2100">belongyng</w>
     <w lemma="to" pos="acp" xml:id="A21658-001-a-2110">to</w>
     <w lemma="her" pos="po" xml:id="A21658-001-a-2120">her</w>
     <w lemma="own" pos="d" reg="own" xml:id="A21658-001-a-2130">owne</w>
     <w lemma="natural" pos="j" reg="natural" xml:id="A21658-001-a-2140">naturall</w>
     <w lemma="subject" pos="n2-j" xml:id="A21658-001-a-2150">Subiectes</w>
     <pc unit="sentence" xml:id="A21658-001-a-2160">.</pc>
     <w lemma="and" pos="cc" xml:id="A21658-001-a-2170">And</w>
     <w lemma="if" pos="cs" xml:id="A21658-001-a-2180">yf</w>
     <w lemma="any" pos="d" xml:id="A21658-001-a-2190">any</w>
     <w lemma="shall" pos="vmb" xml:id="A21658-001-a-2200">shall</w>
     <w lemma="either" pos="d" reg="either" xml:id="A21658-001-a-2210">eyther</w>
     <w lemma="of" pos="acp" xml:id="A21658-001-a-2220">of</w>
     <w lemma="set" pos="j-vn" xml:id="A21658-001-a-2230">set</w>
     <w lemma="purpose" pos="n1" xml:id="A21658-001-a-2240">purpose</w>
     <w lemma="or" pos="cc" xml:id="A21658-001-a-2250">or</w>
     <w lemma="negligence" pos="n1" xml:id="A21658-001-a-2260">negligence</w>
     <w lemma="do" pos="vvd" xml:id="A21658-001-a-2270">do</w>
     <w lemma="to" pos="acp" xml:id="A21658-001-a-2280">to</w>
     <w lemma="the" pos="d" xml:id="A21658-001-a-2290">the</w>
     <w lemma="contrary" pos="n1-j" xml:id="A21658-001-a-2300">contrary</w>
     <pc xml:id="A21658-001-a-2310">:</pc>
     <w lemma="her" pos="po" xml:id="A21658-001-a-2320">her</w>
     <w lemma="majesty" pos="n1" reg="majesty" xml:id="A21658-001-a-2330">Maiestie</w>
     <w lemma="mean" pos="vvz" xml:id="A21658-001-a-2340">meaneth</w>
     <w lemma="to" pos="prt" xml:id="A21658-001-a-2350">to</w>
     <w lemma="cause" pos="vvi" xml:id="A21658-001-a-2360">cause</w>
     <w lemma="the" pos="d" xml:id="A21658-001-a-2370">the</w>
     <w lemma="same" pos="d" xml:id="A21658-001-a-2380">same</w>
     <w lemma="to" pos="prt" xml:id="A21658-001-a-2390">to</w>
     <w lemma="be" pos="vvi" xml:id="A21658-001-a-2400">be</w>
     <w lemma="severe" pos="av-j" reg="severely" xml:id="A21658-001-a-2410">seuerely</w>
     <w lemma="punish" pos="vvn" xml:id="A21658-001-a-2420">punished</w>
     <w lemma="▪" xml:id="A21658-001-a-2430">▪</w>
     <w lemma="as" pos="acp" xml:id="A21658-001-a-2440">as</w>
     <w lemma="the" pos="d" xml:id="A21658-001-a-2450">the</w>
     <w lemma="good" pos="j" xml:id="A21658-001-a-2460">good</w>
     <w lemma="amity" pos="n1" reg="amity" xml:id="A21658-001-a-2470">amitie</w>
     <w lemma="betwixt" pos="acp" xml:id="A21658-001-a-2480">betwixt</w>
     <w lemma="her" pos="po" xml:id="A21658-001-a-2490">her</w>
     <w lemma="majesty" pos="n1" reg="majesty" xml:id="A21658-001-a-2500">Maiestie</w>
     <w lemma="and" pos="cc" xml:id="A21658-001-a-2510">and</w>
     <w lemma="she" pos="pno" xml:id="A21658-001-a-2520">her</w>
     <w lemma="say" pos="vvd" reg="said" xml:id="A21658-001-a-2530">sayde</w>
     <w lemma="good" pos="j" xml:id="A21658-001-a-2540">good</w>
     <w lemma="brother" pos="n1" xml:id="A21658-001-a-2550">brother</w>
     <pc xml:id="A21658-001-a-2560">,</pc>
     <w lemma="do" pos="vvz" xml:id="A21658-001-a-2570">doth</w>
     <w lemma="and" pos="cc" xml:id="A21658-001-a-2580">and</w>
     <w lemma="aught" pos="pi" xml:id="A21658-001-a-2590">ought</w>
     <w lemma="to" pos="prt" xml:id="A21658-001-a-2600">to</w>
     <w lemma="require" pos="vvi" xml:id="A21658-001-a-2610">require</w>
     <pc unit="sentence" xml:id="A21658-001-a-2620">.</pc>
    </p>
    <p xml:id="A21658-e10060">
     <w lemma="give" pos="vvn" xml:id="A21658-001-a-2630">Yeuen</w>
     <w lemma="at" pos="acp" xml:id="A21658-001-a-2640">at</w>
     <w lemma="her" pos="po" xml:id="A21658-001-a-2650">her</w>
     <w lemma="highness" pos="n1" reg="highness" xml:id="A21658-001-a-2660">hyghnes</w>
     <w lemma="castle" pos="n1" reg="castle" xml:id="A21658-001-a-2670">castell</w>
     <w lemma="of" pos="acp" xml:id="A21658-001-a-2680">of</w>
     <w lemma="wyndsor" pos="nn1" reg="windsor" xml:id="A21658-001-a-2690">Wyndsor</w>
     <w lemma="the" pos="d" xml:id="A21658-001-a-2700">the</w>
     <w lemma="first" pos="ord" reg="first" xml:id="A21658-001-a-2710">fyrst</w>
     <w lemma="day" pos="n1" xml:id="A21658-001-a-2720">day</w>
     <w lemma="of" pos="acp" xml:id="A21658-001-a-2730">of</w>
     <w lemma="september" pos="nn1" xml:id="A21658-001-a-2740">September</w>
     <pc xml:id="A21658-001-a-2750">,</pc>
     <w lemma="the" pos="d" xml:id="A21658-001-a-2760">the</w>
     <w lemma="fyfth" pos="ord" reg="fifth" xml:id="A21658-001-a-2770">fyfth</w>
     <w lemma="year" pos="n1" reg="year" xml:id="A21658-001-a-2780">yere</w>
     <w lemma="of" pos="acp" xml:id="A21658-001-a-2790">of</w>
     <w lemma="her" pos="po" xml:id="A21658-001-a-2800">her</w>
     <w lemma="majesty" pos="ng1" reg="majesties" xml:id="A21658-001-a-2810">Maiesties</w>
     <w lemma="reign" pos="n1" reg="reign" xml:id="A21658-001-a-2820">reigne</w>
     <pc unit="sentence" xml:id="A21658-001-a-2830">.</pc>
    </p>
    <p xml:id="A21658-e10070">
     <w lemma="god" pos="nn1" xml:id="A21658-001-a-2840">God</w>
     <w lemma="save" pos="acp" reg="save" xml:id="A21658-001-a-2850">saue</w>
     <w lemma="the" pos="d" xml:id="A21658-001-a-2860">the</w>
     <w lemma="queen" pos="n1" reg="queen" xml:id="A21658-001-a-2870">Quene</w>
     <pc unit="sentence" xml:id="A21658-001-a-2880">.</pc>
    </p>
   </div>
  </body>
  <back xml:id="A21658-e10080">
   <div type="colophon" xml:id="A21658-e10090">
    <p xml:id="A21658-e10100">
     <w lemma="¶" xml:id="A21658-001-a-2890">¶</w>
     <w lemma="imprint" pos="vvn" xml:id="A21658-001-a-2900">Imprinted</w>
     <w lemma="at" pos="acp" xml:id="A21658-001-a-2910">at</w>
     <w lemma="london" pos="nn1" xml:id="A21658-001-a-2920">London</w>
     <w lemma="in" pos="acp" xml:id="A21658-001-a-2930">in</w>
     <w lemma="paul" pos="nng1" xml:id="A21658-001-a-2940">Powles</w>
     <w lemma="churchyard" pos="n1" reg="churchyard" xml:id="A21658-001-a-2960">Churchyarde</w>
     <pc xml:id="A21658-001-a-2980">,</pc>
     <w lemma="by" pos="acp" xml:id="A21658-001-a-2990">by</w>
     <w lemma="richard" pos="nn1" xml:id="A21658-001-a-3000">Richard</w>
     <w lemma="jug" pos="n1" reg="jug" xml:id="A21658-001-a-3010">Iugge</w>
     <w lemma="and" pos="cc" xml:id="A21658-001-a-3020">and</w>
     <w lemma="john" pos="nn1" reg="John" xml:id="A21658-001-a-3030">Iohn</w>
     <w lemma="cawood" pos="nn1" xml:id="A21658-001-a-3040">Cawood</w>
     <pc xml:id="A21658-001-a-3050">,</pc>
     <w lemma="printer" pos="n2" xml:id="A21658-001-a-3060">Printers</w>
     <w lemma="to" pos="acp" xml:id="A21658-001-a-3070">to</w>
     <w lemma="the" pos="d" xml:id="A21658-001-a-3080">the</w>
     <w lemma="queen" pos="ng1" reg="queens" xml:id="A21658-001-a-3090">Quenes</w>
     <w lemma="majesty" pos="n1" reg="majesty" xml:id="A21658-001-a-3100">Maiestie</w>
     <pc unit="sentence" xml:id="A21658-001-a-3110">.</pc>
    </p>
    <p xml:id="A21658-e10110">
     <hi xml:id="A21658-e10120">
      <w lemma="n/a" pos="fla" xml:id="A21658-001-a-3120">Cum</w>
      <w lemma="n/a" pos="fla" reg="privilegio" xml:id="A21658-001-a-3130">priuilegio</w>
      <w lemma="n/a" pos="fla" xml:id="A21658-001-a-3140">Regiae</w>
      <w lemma="n/a" pos="fla" xml:id="A21658-001-a-3150">Maiestatis</w>
     </hi>
     <pc rendition="#follows-hi" unit="sentence" xml:id="A21658-001-a-3160">.</pc>
    </p>
   </div>
  </back>
 </text>
</TEI>
