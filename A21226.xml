<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://schemata.earlyprint.org/schemata/tei_earlyprint.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="A21226">
 <teiHeader>
  <fileDesc>
   <titleStmt>
    <title type="_245">The First anointed Queene I am, within this town which euer came</title>
   </titleStmt>
   <editionStmt>
    <edition>This text is an enriched version of the TCP digital transcription A21226 of text S1547 in the <ref target="A21226-http;//estc.bl.uk">English Short Title Catalog</ref>. Textual changes  and metadata enrichments aim at making the text more  computationally tractable, easier to read, and suitable for network-based collaborative curation by amateur and professional end users from many walks of life.  The text has been tokenized and linguistically annotated with <ref target="http://morphadorner.northwestern.edu/morphadorner/">MorphAdorner</ref>. The annotation includes standard spellings that support the display of a text in	 a standardized format that preserves archaic forms ('loveth', 'seekest'). Textual changes aim at restoring the text the author or stationer meant to publish.</edition>
    <respStmt>
     <resp>This text has not been fully proofread</resp>
     <name/>
    </respStmt>
   </editionStmt>
   <extent>Approx. 7 KB of XML-encoded text transcribed from 2 1-bit group-IV TIFF page images.</extent>
   <publicationStmt>
    <publisher>EarlyPrint Project</publisher>
    <pubPlace>Evanston,IL, Notre Dame, IN, St. Louis, MO</pubPlace>
    <date>2018</date>
    <idno type="DLPS">A21226</idno>
    <idno type="stc">STC 7582.5</idno>
    <idno type="stc">ESTC S1547</idno>
    <idno type="eebo_citation">20212555</idno>
    <idno type="oclc">ocm 20212555</idno>
    <idno type="vid">23813</idno>
    <availability>
     <p>This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. Searching, reading, printing, or downloading EEBO-TCP texts is reserved for the authorized users of these project partner institutions. Permission must be granted for subsequent distribution, in print or electronically, of this text, in whole or in part.  Please contact project staff at eebotcp-info@umich.edu for further information or permissions.</p>
    </availability>
   </publicationStmt>
   <seriesStmt>
    <title>Early English books online.</title>
   </seriesStmt>
   <notesStmt>
    <note>(EEBO-TCP ; phase 2, no. A21226)</note>
    <note>Transcribed from: (Early English Books Online ; image set 23813)</note>
    <note>Images scanned from microfilm: (Early English books, 1475-1640 ; 1709:8)</note>
   </notesStmt>
   <sourceDesc>
    <biblFull>
     <titleStmt>
      <title type="_245">The First anointed Queene I am, within this town which euer came</title>
     </titleStmt>
     <extent>1 broadside.   </extent>
     <publicationStmt>
      <publisher>J. Allde,</publisher>
      <pubPlace>[London? :</pubPlace>
      <date>1573?]</date>
     </publicationStmt>
     <notesStmt>
      <note>In verse.</note>
      <note>First line of text: "O happy town, o happy Rye."</note>
      <note>Publisher and date of publication suggested by STC (2nd ed.).</note>
      <note>Reproduction of original in the Harvard University. Library.</note>
     </notesStmt>
    </biblFull>
   </sourceDesc>
  </fileDesc>
  <encodingDesc>
   <projectDesc>
    <p>Header created with script mrcb2eeboutf.xsl on 2014-03-05.</p>
   </projectDesc>
   <editorialDecl n="4">
    <p>Manually keyed and coded text linked to page images in accordance with level 4 of the TEI in Libraries guidelines.</p>
    <p>Issued variously as SGML (TCP schema; ASCII text with mnemonic sdata character entities); displayable XML (TCP schema; characters represented either as UTF-8 Unicode or text strings within braces); or lossless XML (TEI P5, characters represented either as UTF-8 Unicode or TEI g elements).</p>
    <p>Keying and markup guidelines available at TCP web site (http://www.textcreationpartnership.org/docs/)</p>
   </editorialDecl>
  </encodingDesc>
  <profileDesc>
   <langUsage>
    <language ident="eng">eng</language>
   </langUsage>
   <textClass>
    <keywords scheme="lcsh">
     <term>Elizabeth --  I, --  Queen of England, 1533-1603 --  Poetry.</term>
     <term>Rye (England) --  Poetry.</term>
    </keywords>
   </textClass>
  </profileDesc>
  <xenoData>
   <ep:epHeader xmlns:ep="http://earlyprint.org/ns/1.0">
    <ep:corpus>eebo</ep:corpus>
    <ep:title>The First anointed Queene I am, within this town which euer came</ep:title>
    <ep:author>n/a</ep:author>
    <ep:publicationYear>1573</ep:publicationYear>
    <ep:creationYear/>
    <ep:curator>
     <ep:name/>
    </ep:curator>
    <ep:pageCount>2</ep:pageCount>
    <ep:wordCount>409</ep:wordCount>
    <ep:defectiveTokenCount>0</ep:defectiveTokenCount>
    <ep:untranscribedForeignCount>0</ep:untranscribedForeignCount>
    <ep:untranscribedMathCount>0</ep:untranscribedMathCount>
    <ep:untranscribedMusicCount>0</ep:untranscribedMusicCount>
    <ep:missingChunkCount>0</ep:missingChunkCount>
    <ep:missingPagesCount>0</ep:missingPagesCount>
    <ep:defectRate>0</ep:defectRate>
    <ep:finalGrade>A</ep:finalGrade>
    <ep:defectRangePerGrade>This text  has no known defects that were recorded as gap elements at the time of transcription. </ep:defectRangePerGrade>
   </ep:epHeader>
  </xenoData>
  <revisionDesc>
   <change>
    <date>2012-08</date>
    <label>TCP</label>Assigned for keying and markup
      </change>
   <change>
    <date>2012-08</date>
    <label>SPi Global</label>Keyed and coded from ProQuest page images
      </change>
   <change>
    <date>2012-11</date>
    <label>Mona Logarbo</label>Sampled and proofread
      </change>
   <change>
    <date>2012-11</date>
    <label>Mona Logarbo</label>Text and markup reviewed and edited
      </change>
   <change>
    <date>2013-02</date>
    <label>pfs</label>Batch review (QC) and XML conversion
      </change>
  </revisionDesc>
 </teiHeader>
 <text xml:id="A21226-e10010" xml:lang="eng">
  <front xml:id="A21226-e10020">
   <div type="modern_introduction" xml:id="A21226-e10030">
    <pb facs="tcp:23813:1" xml:id="A21226-001-a"/>
    <head xml:id="A21226-e10040">
     <w lemma="n/a" pos="fla" xml:id="A21226-001-a-0010">UNIQUE</w>
     <w lemma="poetical" pos="j" xml:id="A21226-001-a-0020">POETICAL</w>
     <w lemma="broadside" pos="n1" xml:id="A21226-001-a-0030">BROADSIDE</w>
     <pc unit="sentence" xml:id="A21226-001-a-0040">.</pc>
    </head>
    <head type="sub" xml:id="A21226-e10050">
     <w lemma="146." pos="crd" xml:id="A21226-001-a-0050">146.</w>
     <pc unit="sentence" xml:id="A21226-001-a-0060"/>
     <w lemma="elizabeth" pos="nn1" xml:id="A21226-001-a-0070">ELIZABETH</w>
     <hi xml:id="A21226-e10060">
      <pc xml:id="A21226-001-a-0080">(</pc>
      <w lemma="queen" pos="n1" xml:id="A21226-001-a-0090">Queen</w>
      <w lemma="of" pos="acp" xml:id="A21226-001-a-0100">of</w>
      <w lemma="england" pos="nn1" xml:id="A21226-001-a-0110">England</w>
      <pc xml:id="A21226-001-a-0120">)</pc>
     </hi>
     <pc rendition="#follows-hi" unit="sentence" xml:id="A21226-001-a-0130">.</pc>
    </head>
    <quote xml:id="A21226-e10070">
     <l xml:id="A21226-e10080">
      <w lemma="..." xml:id="A21226-001-a-0140">...</w>
      <w lemma="o" pos="sy" xml:id="A21226-001-a-0150">O</w>
      <w lemma="happy" pos="j" xml:id="A21226-001-a-0160">happy</w>
      <w lemma="town" pos="n1" xml:id="A21226-001-a-0170">town</w>
      <pc xml:id="A21226-001-a-0180">,</pc>
      <w lemma="oh" pos="uh" xml:id="A21226-001-a-0190">O</w>
      <w lemma="happy" pos="j" xml:id="A21226-001-a-0200">happy</w>
      <w lemma="rye" pos="n1" xml:id="A21226-001-a-0210">Rye</w>
      <pc xml:id="A21226-001-a-0220">:</pc>
     </l>
     <l xml:id="A21226-e10090">
      <w lemma="that" pos="cs" xml:id="A21226-001-a-0230">That</w>
      <w lemma="once" pos="acp" xml:id="A21226-001-a-0240">once</w>
      <w lemma="in" pos="acp" xml:id="A21226-001-a-0250">in</w>
      <w lemma="thou" pos="pno" xml:id="A21226-001-a-0260">thee</w>
      <w lemma="you" pos="pn" xml:id="A21226-001-a-0270">ye</w>
      <w lemma="queen" pos="n1" xml:id="A21226-001-a-0280">Queen</w>
      <w lemma="do" pos="vvz" xml:id="A21226-001-a-0290">doth</w>
      <w lemma="lie" pos="vvi" xml:id="A21226-001-a-0300">ly</w>
     </l>
     <l xml:id="A21226-e10100">
      <w lemma="such" pos="d" xml:id="A21226-001-a-0310">Such</w>
      <w lemma="joy" pos="n1" xml:id="A21226-001-a-0320">ioy</w>
      <w lemma="before" pos="acp" xml:id="A21226-001-a-0330">before</w>
      <w lemma="be" pos="vvd" xml:id="A21226-001-a-0340">was</w>
      <w lemma="never" pos="avx" reg="never" xml:id="A21226-001-a-0350">neuer</w>
      <w lemma="see" pos="vvn" xml:id="A21226-001-a-0360">seen</w>
     </l>
     <l xml:id="A21226-e10110">
      <w lemma="in" pos="acp" xml:id="A21226-001-a-0370">In</w>
      <w lemma="rye" pos="n1" xml:id="A21226-001-a-0380">Rye</w>
      <w lemma="as" pos="acp" xml:id="A21226-001-a-0390">as</w>
      <w lemma="now" pos="av" xml:id="A21226-001-a-0400">now</w>
      <w lemma="to" pos="prt" xml:id="A21226-001-a-0410">to</w>
      <w lemma="lodge" pos="vvi" xml:id="A21226-001-a-0420">lodge</w>
      <w lemma="the" pos="d" xml:id="A21226-001-a-0430">the</w>
      <w lemma="queen" pos="n1" xml:id="A21226-001-a-0440">Queen</w>
      <w lemma="...." xml:id="A21226-001-a-0450">....</w>
     </l>
    </quote>
    <p xml:id="A21226-e10120">
     <w lemma="a" pos="d" xml:id="A21226-001-a-0460">A</w>
     <w lemma="single" pos="j" xml:id="A21226-001-a-0470">single</w>
     <w lemma="leaf" pos="n1" xml:id="A21226-001-a-0480">leaf</w>
     <pc xml:id="A21226-001-a-0490">,</pc>
     <w lemma="on" pos="acp" xml:id="A21226-001-a-0500">on</w>
     <w lemma="which" pos="crq" xml:id="A21226-001-a-0510">which</w>
     <w lemma="be" pos="vvz" xml:id="A21226-001-a-0520">is</w>
     <w lemma="print" pos="vvn" xml:id="A21226-001-a-0530">printed</w>
     <w lemma="a" pos="d" xml:id="A21226-001-a-0540">a</w>
     <w lemma="poem" pos="n1" xml:id="A21226-001-a-0550">poem</w>
     <w lemma="of" pos="acp" xml:id="A21226-001-a-0560">of</w>
     <w lemma="fourteen" pos="crd" xml:id="A21226-001-a-0570">fourteen</w>
     <w lemma="line" pos="n2" xml:id="A21226-001-a-0580">lines</w>
     <w lemma="in" pos="acp" xml:id="A21226-001-a-0590">in</w>
     <w lemma="black" pos="j" xml:id="A21226-001-a-0600">Black</w>
     <w lemma="letter" pos="n1" xml:id="A21226-001-a-0610">Letter</w>
     <pc xml:id="A21226-001-a-0620">;</pc>
     <w lemma="reverse" pos="vvb" xml:id="A21226-001-a-0630">reverse</w>
     <w lemma="of" pos="acp" xml:id="A21226-001-a-0640">of</w>
     <w lemma="the" pos="d" xml:id="A21226-001-a-0650">the</w>
     <w lemma="sheet" pos="n1" xml:id="A21226-001-a-0660">sheet</w>
     <w lemma="blank" pos="j" xml:id="A21226-001-a-0670">blank</w>
     <pc xml:id="A21226-001-a-0680">,</pc>
     <w lemma="sm" pos="n1" xml:id="A21226-001-a-0690">sm</w>
     <pc unit="sentence" xml:id="A21226-001-a-0700">.</pc>
     <w lemma="4to" pos="ord" xml:id="A21226-001-a-0710">4to</w>
     <pc xml:id="A21226-001-a-0730">.</pc>
     <pc rendition="#follows-hi" xml:id="A21226-001-a-0740">,</pc>
     <w lemma="uncut" pos="j" xml:id="A21226-001-a-0750">uncut</w>
     <pc xml:id="A21226-001-a-0760">,</pc>
     <w lemma="as" pos="acp" xml:id="A21226-001-a-0770">as</w>
     <w lemma="issue" pos="vvn" xml:id="A21226-001-a-0780">issued</w>
     <pc unit="sentence" xml:id="A21226-001-a-0790">.</pc>
    </p>
    <p xml:id="A21226-e10130">
     <pc xml:id="A21226-001-a-0800">[</pc>
     <w lemma="london" pos="nn1" rendition="#hi" xml:id="A21226-001-a-0810">London</w>
     <pc unit="sentence" xml:id="A21226-001-a-0820">?</pc>
     <w lemma="1573" pos="crd" xml:id="A21226-001-a-0830">1573</w>
     <pc unit="sentence" xml:id="A21226-001-a-0840">]</pc>
    </p>
    <p xml:id="A21226-e10140">
     <w lemma="a" pos="d" xml:id="A21226-001-a-0850">AN</w>
     <w lemma="n/a" pos="fla" xml:id="A21226-001-a-0860">UNIQUE</w>
     <w lemma="poetical" pos="j" xml:id="A21226-001-a-0870">POETICAL</w>
     <w lemma="broadside" pos="n1" xml:id="A21226-001-a-0880">BROADSIDE</w>
     <w lemma="celebrate" pos="vvg" xml:id="A21226-001-a-0890">celebrating</w>
     <w lemma="queen" pos="n1" xml:id="A21226-001-a-0900">Queen</w>
     <w lemma="elizabeth" pos="nng1" xml:id="A21226-001-a-0910">Elizabeth's</w>
     <w lemma="visit" pos="n1" xml:id="A21226-001-a-0920">visit</w>
     <w lemma="to" pos="acp" xml:id="A21226-001-a-0930">to</w>
     <w lemma="rye" pos="n1" xml:id="A21226-001-a-0940">Rye</w>
     <pc xml:id="A21226-001-a-0950">(</pc>
     <w lemma="sussex" pos="nn1" xml:id="A21226-001-a-0960">Sussex</w>
     <pc xml:id="A21226-001-a-0970">)</pc>
     <w lemma="in" pos="acp" xml:id="A21226-001-a-0980">in</w>
     <w lemma="august" pos="nn1" xml:id="A21226-001-a-0990">August</w>
     <pc xml:id="A21226-001-a-1000">,</pc>
     <w lemma="1573." pos="crd" xml:id="A21226-001-a-1010">1573.</w>
     <pc unit="sentence" xml:id="A21226-001-a-1020"/>
     <w lemma="we" pos="pns" xml:id="A21226-001-a-1030">We</w>
     <w lemma="have" pos="vvb" xml:id="A21226-001-a-1040">have</w>
     <pc xml:id="A21226-001-a-1050">,</pc>
     <w lemma="after" pos="acp" xml:id="A21226-001-a-1060">after</w>
     <w lemma="exhaustive" pos="j" xml:id="A21226-001-a-1070">exhaustive</w>
     <w lemma="research" pos="vvi" xml:id="A21226-001-a-1080">research</w>
     <pc xml:id="A21226-001-a-1090">,</pc>
     <w lemma="fail" pos="vvd" xml:id="A21226-001-a-1100">failed</w>
     <w lemma="to" pos="prt" xml:id="A21226-001-a-1110">to</w>
     <w lemma="trace" pos="vvi" xml:id="A21226-001-a-1120">trace</w>
     <w lemma="another" pos="d" xml:id="A21226-001-a-1130">another</w>
     <w lemma="copy" pos="n1" xml:id="A21226-001-a-1140">copy</w>
     <w lemma="of" pos="acp" xml:id="A21226-001-a-1150">of</w>
     <w lemma="this" pos="d" xml:id="A21226-001-a-1160">this</w>
     <w lemma="leaflet" pos="nn1" xml:id="A21226-001-a-1170">leaflet</w>
     <pc xml:id="A21226-001-a-1180">;</pc>
     <w lemma="there" pos="av" xml:id="A21226-001-a-1190">there</w>
     <w lemma="be" pos="vvz" xml:id="A21226-001-a-1200">is</w>
     <w lemma="no" pos="dx" xml:id="A21226-001-a-1210">no</w>
     <w lemma="mention" pos="n1" xml:id="A21226-001-a-1220">mention</w>
     <w lemma="of" pos="acp" xml:id="A21226-001-a-1230">of</w>
     <w lemma="it" pos="pn" xml:id="A21226-001-a-1240">it</w>
     <w lemma="in" pos="acp" xml:id="A21226-001-a-1250">in</w>
     <w lemma="any" pos="d" xml:id="A21226-001-a-1260">any</w>
     <w lemma="work" pos="n1" xml:id="A21226-001-a-1270">work</w>
     <w lemma="of" pos="acp" xml:id="A21226-001-a-1280">of</w>
     <w lemma="reference" pos="n1" xml:id="A21226-001-a-1290">reference</w>
     <w lemma="or" pos="cc" xml:id="A21226-001-a-1300">or</w>
     <w lemma="library" pos="n1" xml:id="A21226-001-a-1310">library</w>
     <w lemma="catalogue" pos="n1" xml:id="A21226-001-a-1320">catalogue</w>
     <pc unit="sentence" xml:id="A21226-001-a-1330">.</pc>
     <w lemma="this" pos="d" xml:id="A21226-001-a-1340">This</w>
     <w lemma="broadside" pos="n1" xml:id="A21226-001-a-1350">broadside</w>
     <w lemma="be" pos="vvz" xml:id="A21226-001-a-1360">is</w>
     <w lemma="all" pos="d" xml:id="A21226-001-a-1370">all</w>
     <w lemma="the" pos="d" xml:id="A21226-001-a-1380">the</w>
     <w lemma="more" pos="avc-d" xml:id="A21226-001-a-1390">more</w>
     <w lemma="interest" pos="nn1" xml:id="A21226-001-a-1400">interesting</w>
     <w lemma="since" pos="acp" xml:id="A21226-001-a-1410">since</w>
     <w lemma="scarce" pos="av-j" xml:id="A21226-001-a-1420">scarcely</w>
     <w lemma="anything" pos="pi" xml:id="A21226-001-a-1430">anything</w>
     <w lemma="be" pos="vvz" xml:id="A21226-001-a-1440">is</w>
     <w lemma="know" pos="vvn" xml:id="A21226-001-a-1450">known</w>
     <w lemma="about" pos="acp" xml:id="A21226-001-a-1460">about</w>
     <w lemma="the" pos="d" xml:id="A21226-001-a-1470">the</w>
     <w lemma="visit" pos="n1" xml:id="A21226-001-a-1480">visit</w>
     <w lemma="it" pos="pn" xml:id="A21226-001-a-1490">it</w>
     <w lemma="commemorate" pos="nn1" xml:id="A21226-001-a-1500">commemorates</w>
     <pc unit="sentence" xml:id="A21226-001-a-1510">.</pc>
     <w lemma="the" pos="d" xml:id="A21226-001-a-1520">The</w>
     <w lemma="late" pos="js" xml:id="A21226-001-a-1530">latest</w>
     <w lemma="historian" pos="n1" xml:id="A21226-001-a-1540">historian</w>
     <w lemma="of" pos="acp" xml:id="A21226-001-a-1550">of</w>
     <w lemma="the" pos="d" xml:id="A21226-001-a-1560">the</w>
     <w lemma="town" pos="n1" xml:id="A21226-001-a-1570">town</w>
     <w lemma="write" pos="vvz" xml:id="A21226-001-a-1580">writes</w>
     <w lemma="as" pos="acp" xml:id="A21226-001-a-1590">as</w>
     <w lemma="follow" pos="vvz" xml:id="A21226-001-a-1600">follows</w>
     <pc xml:id="A21226-001-a-1610">:</pc>
     <pc xml:id="A21226-001-a-1620">"</pc>
     <w lemma="we" pos="pns" xml:id="A21226-001-a-1630">We</w>
     <w lemma="wish" pos="vvb" xml:id="A21226-001-a-1640">wish</w>
     <w lemma="we" pos="pns" xml:id="A21226-001-a-1650">we</w>
     <w lemma="know" pos="vvd" xml:id="A21226-001-a-1660">knew</w>
     <w lemma="more" pos="dc" xml:id="A21226-001-a-1670">more</w>
     <w lemma="of" pos="acp" xml:id="A21226-001-a-1680">of</w>
     <w lemma="the" pos="d" xml:id="A21226-001-a-1690">the</w>
     <w lemma="detail" pos="vvz" xml:id="A21226-001-a-1700">details</w>
     <w lemma="of" pos="acp" xml:id="A21226-001-a-1710">of</w>
     <w lemma="her" pos="po" xml:id="A21226-001-a-1720">her</w>
     <w lemma="visit" pos="n1" xml:id="A21226-001-a-1730">visit</w>
     <pc xml:id="A21226-001-a-1740">,</pc>
     <w lemma="but" pos="acp" xml:id="A21226-001-a-1750">but</w>
     <w lemma="tradition" pos="n1" xml:id="A21226-001-a-1760">tradition</w>
     <w lemma="tell" pos="vvz" xml:id="A21226-001-a-1770">tells</w>
     <w lemma="we" pos="pno" xml:id="A21226-001-a-1780">us</w>
     <w lemma="that" pos="cs" xml:id="A21226-001-a-1790">that</w>
     <w lemma="she" pos="pns" xml:id="A21226-001-a-1800">she</w>
     <w lemma="drink" pos="vvd" xml:id="A21226-001-a-1810">drank</w>
     <w lemma="of" pos="acp" xml:id="A21226-001-a-1820">of</w>
     <w lemma="the" pos="d" xml:id="A21226-001-a-1830">the</w>
     <w lemma="water" pos="n1" xml:id="A21226-001-a-1840">water</w>
     <w lemma="of" pos="acp" xml:id="A21226-001-a-1850">of</w>
     <w lemma="queenswell" pos="nn1" xml:id="A21226-001-a-1860">Queenswell</w>
     <pc xml:id="A21226-001-a-1870">;</pc>
     <w lemma="some" pos="d" xml:id="A21226-001-a-1880">some</w>
     <w lemma="say" pos="vvb" xml:id="A21226-001-a-1890">say</w>
     <w lemma="she" pos="pns" xml:id="A21226-001-a-1900">she</w>
     <w lemma="and" pos="cc" xml:id="A21226-001-a-1910">and</w>
     <w lemma="her" pos="po" xml:id="A21226-001-a-1920">her</w>
     <w lemma="train" pos="n1" xml:id="A21226-001-a-1930">train</w>
     <w lemma="have" pos="vvd" xml:id="A21226-001-a-1940">had</w>
     <w lemma="a" pos="d" xml:id="A21226-001-a-1950">an</w>
     <w lemma="alfresco" pos="nn1" rend="hi" xml:id="A21226-001-a-1960">alfresco</w>
     <w lemma="meal" pos="n1" xml:id="A21226-001-a-1970">meal</w>
     <w lemma="there" pos="av" xml:id="A21226-001-a-1980">there</w>
     <w lemma="and" pos="cc" xml:id="A21226-001-a-1990">and</w>
     <w lemma="come" pos="vvd" xml:id="A21226-001-a-2000">came</w>
     <w lemma="through" pos="acp" xml:id="A21226-001-a-2010">through</w>
     <w lemma="the" pos="d" xml:id="A21226-001-a-2020">the</w>
     <w lemma="postern" pos="n1" xml:id="A21226-001-a-2030">Postern</w>
     <w lemma="gate" pos="n1" xml:id="A21226-001-a-2040">Gate</w>
     <pc xml:id="A21226-001-a-2050">,</pc>
     <w lemma="where" pos="crq" xml:id="A21226-001-a-2060">where</w>
     <w lemma="she" pos="pns" xml:id="A21226-001-a-2070">she</w>
     <w lemma="be" pos="vvd" xml:id="A21226-001-a-2080">was</w>
     <w lemma="meet" pos="vvn" xml:id="A21226-001-a-2090">met</w>
     <w lemma="by" pos="acp" xml:id="A21226-001-a-2100">by</w>
     <w lemma="the" pos="d" xml:id="A21226-001-a-2110">the</w>
     <w lemma="mayor" pos="n1" xml:id="A21226-001-a-2120">Mayor</w>
     <w lemma="and" pos="cc" xml:id="A21226-001-a-2130">and</w>
     <w lemma="jurat" pos="n2" xml:id="A21226-001-a-2140">Jurats</w>
     <pc xml:id="A21226-001-a-2150">,</pc>
     <w lemma="who" pos="crq" xml:id="A21226-001-a-2160">who</w>
     <w lemma="escort" pos="vvd" xml:id="A21226-001-a-2170">escorted</w>
     <w lemma="she" pos="pno" xml:id="A21226-001-a-2180">her</w>
     <w lemma="into" pos="acp" xml:id="A21226-001-a-2190">into</w>
     <w lemma="the" pos="d" xml:id="A21226-001-a-2200">the</w>
     <w lemma="town" pos="n1" xml:id="A21226-001-a-2210">town</w>
     <w lemma="and" pos="cc" xml:id="A21226-001-a-2220">and</w>
     <w lemma="give" pos="vvd" xml:id="A21226-001-a-2230">gave</w>
     <w lemma="she" pos="pno" xml:id="A21226-001-a-2240">her</w>
     <w lemma="a" pos="d" xml:id="A21226-001-a-2250">a</w>
     <w lemma="royal" pos="j" xml:id="A21226-001-a-2260">royal</w>
     <w lemma="welcome" pos="n1-j" xml:id="A21226-001-a-2270">welcome</w>
     <pc unit="sentence" xml:id="A21226-001-a-2280">.</pc>
     <w lemma="the" pos="d" xml:id="A21226-001-a-2290">The</w>
     <w lemma="date" pos="n1" xml:id="A21226-001-a-2300">date</w>
     <w lemma="of" pos="acp" xml:id="A21226-001-a-2310">of</w>
     <w lemma="her" pos="po" xml:id="A21226-001-a-2320">her</w>
     <w lemma="arrival" pos="n1" xml:id="A21226-001-a-2330">arrival</w>
     <w lemma="will" pos="vmd" xml:id="A21226-001-a-2340">would</w>
     <w lemma="seem" pos="vvi" xml:id="A21226-001-a-2350">seem</w>
     <w lemma="to" pos="prt" xml:id="A21226-001-a-2360">to</w>
     <w lemma="be" pos="vvi" xml:id="A21226-001-a-2370">be</w>
     <w lemma="11th" pos="ord" xml:id="A21226-001-a-2380">11th</w>
     <w lemma="august" pos="nn1" xml:id="A21226-001-a-2390">August</w>
     <pc xml:id="A21226-001-a-2400">,</pc>
     <w lemma="1573." pos="crd" xml:id="A21226-001-a-2410">1573.</w>
     <pc unit="sentence" xml:id="A21226-001-a-2420">"</pc>
     <pc xml:id="A21226-001-a-2430">(</pc>
     <w lemma="Vidler" pos="nn1" xml:id="A21226-001-a-2440">Vidler</w>
     <pc xml:id="A21226-001-a-2450">:</pc>
     <hi xml:id="A21226-e10150">
      <w lemma="a" pos="d" xml:id="A21226-001-a-2460">A</w>
      <w lemma="new" pos="j" xml:id="A21226-001-a-2470">New</w>
      <w lemma="history" pos="n1" xml:id="A21226-001-a-2480">History</w>
      <w lemma="of" pos="acp" xml:id="A21226-001-a-2490">of</w>
      <w lemma="rye" pos="n1" xml:id="A21226-001-a-2500">Rye</w>
     </hi>
     <pc rendition="#follows-hi" xml:id="A21226-001-a-2510">,</pc>
     <w lemma="p." pos="ab" xml:id="A21226-001-a-2520">p.</w>
     <w lemma="63." pos="crd" xml:id="A21226-001-a-2530">63.</w>
     <pc unit="sentence" xml:id="A21226-001-a-2540">)</pc>
    </p>
    <p xml:id="A21226-e10160">
     <w lemma="this" pos="d" xml:id="A21226-001-a-2550">This</w>
     <w lemma="broadside" pos="n1" xml:id="A21226-001-a-2560">broadside</w>
     <w lemma="be" pos="vvd" xml:id="A21226-001-a-2570">was</w>
     <w lemma="probable" pos="av-j" xml:id="A21226-001-a-2580">probably</w>
     <w lemma="print" pos="vvn" xml:id="A21226-001-a-2590">printed</w>
     <w lemma="in" pos="acp" xml:id="A21226-001-a-2600">in</w>
     <w lemma="london" pos="nn1" xml:id="A21226-001-a-2610">London</w>
     <w lemma="and" pos="cc" xml:id="A21226-001-a-2620">and</w>
     <w lemma="sell" pos="vvn" xml:id="A21226-001-a-2630">sold</w>
     <w lemma="in" pos="acp" xml:id="A21226-001-a-2640">in</w>
     <w lemma="the" pos="d" xml:id="A21226-001-a-2650">the</w>
     <w lemma="street" pos="n2" xml:id="A21226-001-a-2660">streets</w>
     <w lemma="of" pos="acp" xml:id="A21226-001-a-2670">of</w>
     <w lemma="rye" pos="n1" xml:id="A21226-001-a-2680">Rye</w>
     <w lemma="immediate" pos="av-j" xml:id="A21226-001-a-2690">immediately</w>
     <w lemma="before" pos="acp" xml:id="A21226-001-a-2700">before</w>
     <w lemma="the" pos="d" xml:id="A21226-001-a-2710">the</w>
     <w lemma="queen" pos="ng1" xml:id="A21226-001-a-2720">Queen's</w>
     <w lemma="arrival" pos="n1" xml:id="A21226-001-a-2730">arrival</w>
     <pc unit="sentence" xml:id="A21226-001-a-2740">.</pc>
    </p>
   </div>
  </front>
  <body xml:id="A21226-e10170">
   <div type="poem" xml:id="A21226-e10180">
    <pb facs="tcp:23813:2" xml:id="A21226-002-a"/>
    <argument xml:id="A21226-e10190">
     <lg xml:id="A21226-e10200">
      <l xml:id="A21226-e10210">
       <w lemma="the" pos="d" xml:id="A21226-002-a-0010">The</w>
       <w lemma="first" pos="ord" xml:id="A21226-002-a-0020">first</w>
       <w lemma="anoint" pos="vvd" xml:id="A21226-002-a-0030">anointed</w>
       <w lemma="queen" pos="n1" reg="queen" xml:id="A21226-002-a-0040">Queene</w>
       <w lemma="i" pos="pns" xml:id="A21226-002-a-0050">I</w>
       <w lemma="be" pos="vvm" xml:id="A21226-002-a-0060">am</w>
       <pc xml:id="A21226-002-a-0070">:</pc>
      </l>
      <l xml:id="A21226-e10220">
       <w lemma="within" pos="acp" xml:id="A21226-002-a-0080">Within</w>
       <w lemma="this" pos="d" xml:id="A21226-002-a-0090">this</w>
       <w lemma="town" pos="n1" xml:id="A21226-002-a-0100">town</w>
       <w lemma="which" pos="crq" xml:id="A21226-002-a-0110">which</w>
       <w lemma="ever" pos="av" reg="ever" xml:id="A21226-002-a-0120">euer</w>
       <w lemma="come" pos="vvd" xml:id="A21226-002-a-0130">came</w>
       <pc unit="sentence" xml:id="A21226-002-a-0140">.</pc>
      </l>
     </lg>
    </argument>
    <lg xml:id="A21226-e10230">
     <head xml:id="A21226-e10240">
      <w lemma="¶" xml:id="A21226-002-a-0150">¶</w>
      <w lemma="a" pos="d" xml:id="A21226-002-a-0160">A</w>
      <w lemma="say" pos="n1-vg" xml:id="A21226-002-a-0170">saying</w>
      <w lemma="of" pos="acp" xml:id="A21226-002-a-0180">of</w>
      <w lemma="each" pos="d" xml:id="A21226-002-a-0190">each</w>
      <w lemma="good" pos="j" xml:id="A21226-002-a-0200">good</w>
      <w lemma="subject" pos="n1-j" reg="subject" xml:id="A21226-002-a-0210">Subiect</w>
      <w lemma="of" pos="acp" xml:id="A21226-002-a-0220">of</w>
      <w lemma="rye" pos="n1" xml:id="A21226-002-a-0230">Rye</w>
      <pc unit="sentence" xml:id="A21226-002-a-0240">.</pc>
     </head>
     <l xml:id="A21226-e10250">
      <w lemma="o" pos="sy" rend="decorinit" xml:id="A21226-002-a-0250">O</w>
      <w lemma="happy" pos="j" xml:id="A21226-002-a-0260">Happy</w>
      <w lemma="town" pos="n1" xml:id="A21226-002-a-0270">town</w>
      <pc xml:id="A21226-002-a-0280">,</pc>
      <w lemma="oh" pos="uh" xml:id="A21226-002-a-0290">O</w>
      <w lemma="happy" pos="j" xml:id="A21226-002-a-0300">happy</w>
      <w lemma="rye" pos="n1" xml:id="A21226-002-a-0310">Rye</w>
      <pc xml:id="A21226-002-a-0320">:</pc>
     </l>
     <l xml:id="A21226-e10260">
      <w lemma="that" pos="cs" xml:id="A21226-002-a-0330">that</w>
      <w lemma="once" pos="acp" xml:id="A21226-002-a-0340">once</w>
      <w lemma="in" pos="acp" xml:id="A21226-002-a-0350">in</w>
      <w lemma="thou" pos="pno" xml:id="A21226-002-a-0360">thee</w>
      <w lemma="the" orig="yᵉ" pos="d" xml:id="A21226-002-a-0370">the</w>
      <w lemma="queen" pos="n1" xml:id="A21226-002-a-0390">Queen</w>
      <w lemma="doth" pos="av" xml:id="A21226-002-a-0400">dothly</w>
     </l>
     <l xml:id="A21226-e10270">
      <w lemma="such" pos="d" xml:id="A21226-002-a-0410">Such</w>
      <w lemma="joy" pos="n1" xml:id="A21226-002-a-0420">ioy</w>
      <w lemma="before" pos="acp" xml:id="A21226-002-a-0430">before</w>
      <w lemma="be" pos="vvd" xml:id="A21226-002-a-0440">was</w>
      <w lemma="never" pos="avx" reg="never" xml:id="A21226-002-a-0450">neuer</w>
      <w lemma="see" pos="vvn" xml:id="A21226-002-a-0460">seen</w>
     </l>
     <l xml:id="A21226-e10280">
      <w lemma="in" pos="acp" xml:id="A21226-002-a-0470">In</w>
      <w lemma="rye" pos="n1" xml:id="A21226-002-a-0480">Rye</w>
      <w lemma="as" pos="acp" xml:id="A21226-002-a-0490">as</w>
      <w lemma="now" pos="av" xml:id="A21226-002-a-0500">now</w>
      <w lemma="to" pos="prt" xml:id="A21226-002-a-0510">to</w>
      <w lemma="lodge" pos="vvi" xml:id="A21226-002-a-0520">lodge</w>
      <w lemma="the" pos="d" xml:id="A21226-002-a-0530">the</w>
      <w lemma="queen" pos="n1" xml:id="A21226-002-a-0540">Queen</w>
      <pc unit="sentence" xml:id="A21226-002-a-0550">.</pc>
     </l>
     <l xml:id="A21226-e10290">
      <w lemma="you" pos="pn" xml:id="A21226-002-a-0560">You</w>
      <w lemma="fisher" pos="n1" xml:id="A21226-002-a-0570">fissher</w>
      <w lemma="man" pos="n2" xml:id="A21226-002-a-0580">men</w>
      <w lemma="of" pos="acp" xml:id="A21226-002-a-0590">of</w>
      <w lemma="rye" pos="n1" xml:id="A21226-002-a-0600">Rye</w>
      <w lemma="rejoice" pos="vvi" reg="rejoice" xml:id="A21226-002-a-0610">reioyce</w>
      <pc xml:id="A21226-002-a-0620">:</pc>
     </l>
     <l xml:id="A21226-e10300">
      <w lemma="to" pos="prt" xml:id="A21226-002-a-0630">To</w>
      <w lemma="see" pos="vvi" xml:id="A21226-002-a-0640">see</w>
      <w lemma="your" pos="po" xml:id="A21226-002-a-0650">your</w>
      <w lemma="queen" pos="n1" xml:id="A21226-002-a-0660">Queen</w>
      <w lemma="&amp;" pos="cc" xml:id="A21226-002-a-0670">&amp;</w>
      <w lemma="hear" pos="vvi" xml:id="A21226-002-a-0680">hear</w>
      <w lemma="her" pos="po" xml:id="A21226-002-a-0690">her</w>
      <w lemma="voice" pos="n1" xml:id="A21226-002-a-0700">voice</w>
      <pc unit="sentence" xml:id="A21226-002-a-0710">.</pc>
     </l>
     <l xml:id="A21226-e10310">
      <w lemma="now" pos="av" xml:id="A21226-002-a-0720">Now</w>
      <w lemma="clap" pos="vvb" xml:id="A21226-002-a-0730">clap</w>
      <w lemma="your" pos="po" xml:id="A21226-002-a-0740">your</w>
      <w lemma="hand" pos="n2" xml:id="A21226-002-a-0750">hands</w>
      <w lemma="rejoice" pos="vvi" reg="rejoice" xml:id="A21226-002-a-0760">reioice</w>
      <w lemma="&amp;" pos="cc" xml:id="A21226-002-a-0770">&amp;</w>
      <w lemma="sing" pos="vvi" xml:id="A21226-002-a-0780">sing</w>
      <pc xml:id="A21226-002-a-0790">:</pc>
     </l>
     <l xml:id="A21226-e10320">
      <w lemma="which" pos="crq" xml:id="A21226-002-a-0800">which</w>
      <w lemma="never" pos="avx" reg="never" xml:id="A21226-002-a-0810">neuer</w>
      <w lemma="erst" pos="av" xml:id="A21226-002-a-0820">erst</w>
      <w lemma="lodge" pos="vvn" xml:id="A21226-002-a-0830">lodged</w>
      <w lemma="queen" pos="n1" xml:id="A21226-002-a-0840">Queen</w>
      <w lemma="ne" pos="ccx" xml:id="A21226-002-a-0850">ne</w>
      <w lemma="king" pos="n1" xml:id="A21226-002-a-0860">king</w>
      <pc unit="sentence" xml:id="A21226-002-a-0870">.</pc>
     </l>
     <l xml:id="A21226-e10330">
      <w lemma="rejoice" pos="vvb" reg="rejoice" xml:id="A21226-002-a-0880">Reioyce</w>
      <w lemma="thou" pos="pns" xml:id="A21226-002-a-0890">thou</w>
      <w lemma="town" pos="n1" xml:id="A21226-002-a-0900">town</w>
      <w lemma="and" pos="cc" xml:id="A21226-002-a-0910">and</w>
      <w lemma="port" pos="n1" reg="port" xml:id="A21226-002-a-0920">porte</w>
      <w lemma="of" pos="acp" xml:id="A21226-002-a-0930">of</w>
      <w lemma="rye" pos="n1" xml:id="A21226-002-a-0940">Rye</w>
      <pc xml:id="A21226-002-a-0950">:</pc>
     </l>
     <l xml:id="A21226-e10340">
      <w lemma="to" pos="prt" xml:id="A21226-002-a-0960">To</w>
      <w lemma="see" pos="vvi" xml:id="A21226-002-a-0970">see</w>
      <w lemma="thy" pos="po" xml:id="A21226-002-a-0980">thy</w>
      <w lemma="sovereign" pos="n2-j" xml:id="A21226-002-a-0990">souerains</w>
      <w lemma="majesty" pos="n1" xml:id="A21226-002-a-1000">Meiestie</w>
      <pc unit="sentence" xml:id="A21226-002-a-1010">.</pc>
     </l>
     <l xml:id="A21226-e10350">
      <w lemma="what" pos="crq" xml:id="A21226-002-a-1020">What</w>
      <w lemma="hart" pos="n1" xml:id="A21226-002-a-1030">hart</w>
      <w lemma="have" pos="vvz" xml:id="A21226-002-a-1040">hath</w>
      <w lemma="he" pos="pns" xml:id="A21226-002-a-1050">he</w>
      <w lemma="that" pos="cs" xml:id="A21226-002-a-1060">that</w>
      <w lemma="dwell" pos="vvz" reg="dwells" xml:id="A21226-002-a-1070">dwelles</w>
      <w lemma="in" pos="acp" xml:id="A21226-002-a-1080">in</w>
      <w lemma="rye" pos="n1" xml:id="A21226-002-a-1090">Rye</w>
      <pc xml:id="A21226-002-a-1100">:</pc>
     </l>
     <l xml:id="A21226-e10360">
      <w lemma="that" pos="cs" xml:id="A21226-002-a-1110">That</w>
      <w lemma="joy" pos="vvz" reg="joys" xml:id="A21226-002-a-1120">ioyes</w>
      <w lemma="not" pos="xx" xml:id="A21226-002-a-1130">not</w>
      <w lemma="now" pos="av" xml:id="A21226-002-a-1140">now</w>
      <w lemma="as" pos="acp" xml:id="A21226-002-a-1150">as</w>
      <w lemma="well" pos="av" reg="well" xml:id="A21226-002-a-1160">wel</w>
      <w lemma="as" pos="acp" xml:id="A21226-002-a-1170">as</w>
      <w lemma="i" pos="pns" xml:id="A21226-002-a-1180">I</w>
      <pc unit="sentence" xml:id="A21226-002-a-1190">?</pc>
     </l>
     <l xml:id="A21226-e10370">
      <w lemma="oh" pos="uh" xml:id="A21226-002-a-1200">Oh</w>
      <w lemma="god" pos="nn1" xml:id="A21226-002-a-1210">God</w>
      <w lemma="that" pos="cs" xml:id="A21226-002-a-1220">that</w>
      <w lemma="give" pos="vv2" reg="givest" xml:id="A21226-002-a-1230">giuest</w>
      <w lemma="life" pos="n1" xml:id="A21226-002-a-1240">life</w>
      <w lemma="and" pos="cc" xml:id="A21226-002-a-1250">and</w>
      <w lemma="breath" pos="n1" reg="breath" xml:id="A21226-002-a-1260">breth</w>
      <pc xml:id="A21226-002-a-1270">:</pc>
     </l>
     <l xml:id="A21226-e10380">
      <w lemma="preserve" pos="vvb" reg="preserve" xml:id="A21226-002-a-1280">Preserue</w>
      <w lemma="our" pos="po" xml:id="A21226-002-a-1290">our</w>
      <w lemma="queen" pos="n1" xml:id="A21226-002-a-1300">Queen</w>
      <w lemma="elizabeth" pos="nn1" xml:id="A21226-002-a-1310">Elizabeth</w>
      <pc unit="sentence" xml:id="A21226-002-a-1320">.</pc>
     </l>
    </lg>
    <epigraph xml:id="A21226-e10390">
     <quote xml:id="A21226-e10400">
      <w lemma="n/a" pos="fla" reg="vivat" xml:id="A21226-002-a-1330">Viuat</w>
      <w lemma="nestorios" pos="nn1" xml:id="A21226-002-a-1340">Nestorios</w>
      <w lemma="elizabetha" pos="nn1" xml:id="A21226-002-a-1350">Elizabetha</w>
      <w lemma="die" pos="vvz" xml:id="A21226-002-a-1360">dies</w>
      <pc unit="sentence" xml:id="A21226-002-a-1370">.</pc>
     </quote>
    </epigraph>
   </div>
  </body>
 </text>
</TEI>
